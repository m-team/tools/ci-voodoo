#!/bin/bash

THIS=$(cd "`dirname $0`" 2>/dev/null && pwd)
BASE=${THIS}/..
. ${BASE}/ci-tools/common-functions.sh

# DOCKER_OPTIONS=""
# VERBOSE=""
# VERSION=1
# PREF=marcvs/build
# VERSION=`date +%y%m%d-%H%M`
VERSION=`date +%y%m%d`
NAME=motley-cue

echo "BASE: $BASE"

$BASE/ci-tools/build-linux-dockers-common.sh --name $NAME $@ --version ${VERSION}

