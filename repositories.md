
The [https://repo.data.kit.edu](https://repo.data.kit.edu) server holds three repositories:

# Production

It's the main repo. (Could be called "stable")

- Packages or Metadata are signed with this key:
    [https://repo.data.kit.edu/repo-data-kit-edu-key.gpg](https://repo.data.kit.edu/repo-data-kit-edu-key.gpg)

```gpg
sec   rsa3072/0x512839863D487A87 2018-11-15 [SC]
      Key fingerprint = ACDF B08F DC96 2044 D87F  F00B 5128 3986 3D48 7A87
uid                   [ultimate] Debian Packages (packages.data.kit.edu) <packages@lists.kit.edu>
```

- The private key is with Diana, Gabriel, and Marcus

- Packages go here from the `pre-release` repo in a manual way

- Existing package versions may **not** be overwritten.

# Pre Release

- Packages or Metadata are signed with the Automatic Repository Signature key:

```gpg
pub   rsa3072 2022-10-19 [SC]
      F35D 4134 4946 F23D 5D77  AFD1 E89B 3987 AEB6 032B
uid           [ unknown] Automatic Repository Signature <m-sec@lists.kit.edu>
```

- Pacakges go here from `devel` if all tests worked in an automatic way

- Existing package versions may **not** be overwritten. Automatic tests
    will be inplace to make sure.

# Development

- Packages or Metadata are signed with the Automatic Repository Signature key:

```gpg
pub   rsa3072 2022-10-19 [SC]
      F35D 4134 4946 F23D 5D77  AFD1 E89B 3987 AEB6 032B
uid           [ unknown] Automatic Repository Signature <m-sec@lists.kit.edu>
```

- The private key is not encrypted and available to whatever runs in the
    pipelines

- Packages go here automatically after built in a pipeline

- **Existing package versions may be overwritten**
