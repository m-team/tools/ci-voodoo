echo off

set VERSION=3
set NAMESPACE=marcvs/build
set NAME="putty"

cp ../ci-tools/windows/win-ci-interface.sh .

REM # Debug run:     docker run --rm -i  marcvs/build_putty_win-msys2:2
REM #                docker run --rm -i  marcvs/build_putty_win-msys2-mingw64-gcc:2 pwsh.exe
REM # Debug compile: docker run --rm -i  marcvs/build_putty_win-msys2:2 powershell --branch dev_4.3.0



set PLATFORM=win-msys2-mingw64
echo ""
echo BUILDING 64bit
echo docker build --tag %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION% -f .\%PLATFORM%\Dockerfile .
docker build --tag %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION% -f .\%PLATFORM%\Dockerfile . >> docker-msys2-mingw64-gcc.log
echo ""
echo ""
echo PUSHING 64bit
echo ""
echo %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%
docker push %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%



set PLATFORM=win-msys2-mingw32

echo ""
echo BUILDING 32bit
echo docker build --tag %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION% -f .\%PLATFORM%\Dockerfile .
docker build --tag %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION% -f .\%PLATFORM%\Dockerfile . >> docker-msys2-mingw64-gcc.log
echo ""
echo ""
echo PUSHING 32bit
echo ""
echo %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%
docker push %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%


REM docker build --tag marcvs/build_putty_win-msys2-mingw32:2   -f .\windows\msys2-mingw32\Dockerfile .


exit 0

# For msys2-mingw32, basically... 
#  - run the docker build command until it fails
#  - run the latest image:
#    docker run -i <image id>
#  - run the commands by hand
#  - exit the container
#  - docker commit <container>
#  - docker tag <image> marcvs/build_putty_win-msys2-mingw32:2
#  - docker push marcvs/build_putty_win-msys2-mingw32:2
#  - docker rm <container>
#
#  Further reading: https://www.dataset.com/blog/create-docker-image/
