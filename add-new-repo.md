- Add distro to ci-voodoo/ci-tools/common-functions.sh
- Add distro to ci-voodoo/ci-include/generic-ci.yaml
- Add distro to ci-voodoo/ci-include/distro-build-conditions.yaml

- create repo info files using (on localhost) 
    - (if RPM: add a template file)
    - run: 
      for i in devel prerel preprod / ; do
          ci-voodoo/ci-tools/repo-info/update-repo -t $i
      done

- sign the empty build folder for the new repos:
      for i in devel prerel preprod  ; do
          ssh cicd@repo.data.kit.edu "~/ci-voodoo/ci-tools/sign-repo.sh -t $i -d opensuse -r 15.6"'
      done
      ssh -R /run/user/1000/gnupg/S.gpg-agent:/run/user/1000/gnupg/S.gpg-agent.extra build@repo.data.kit.edu "~cicd/ci-voodoo/ci-tools/sign-repo.sh -t / -d opensuse -r 15.6 -k prod

- Build docker images (make sure dependencies exist)
  - oidc-acent
  - motley-cue
  - test-ssh?
  - ...

- (as cicd / repo) create folders on repo.data.kit.edu in /var/www

# Typically forgotten

- make sure that build-conditions are properly reflected in triggered
    pipelines. E.g. in
    /m-team/tools/integration/test-ssh-oidc/.gitlab-ci.yml
