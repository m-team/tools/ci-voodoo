echo off

for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%" 
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
set "VERSION=%YY%%MM%%DD%"

set NAMESPACE=marcvs/build
set NAME=ki-oidc-plugin

cp ../ci-tools/windows/win-ci-interface.sh .

REM # Debug run:     docker run --rm -i  marcvs/build_ki-oidc-plugin_win-msys2:2
REM #                docker run --rm -i  marcvs/build_ki-oidc-plugin_win-msys2-mingw64-gcc:2 pwsh.exe
REM # Debug compile: docker run --rm -i  marcvs/build_ki-oidc-plugin_win-msys2:2 powershell --branch dev_4.3.0


set PLATFORM=win-msys2-mingw64

set "TAG=%NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%"
set "TAG_LATEST=%NAMESPACE%_%NAME%_%PLATFORM%:latest"
set "TAG_SHORT=%NAMESPACE%_%NAME%_%PLATFORM%"

echo ""
echo BUILDING %PLATFORM%
echo ""
echo tag: "%TAG%"
echo docker build --tag %TAG% -f .\%PLATFORM%\Dockerfile . >> docker.log
docker build --tag %TAG% -f .\%PLATFORM%\Dockerfile . >> docker.log
echo ""
echo ""
echo PUSHING %TAG%
echo ""
echo docker push %TAG%
docker push %TAG%
echo docker push %TAG_LATEST%
docker image tag %TAG% %TAG_LATEST%
docker push %TAG_LATEST%


exit 0

REM REPEAT THE ABOVE WITH DIFFERENT PLATFORM NAME
REM
set PLATFORM=win-msys2-mingw32

set "TAG=%NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%"
set "TAG_LATEST=%NAMESPACE%_%NAME%_%PLATFORM%:latest"
set "TAG_SHORT=%NAMESPACE%_%NAME%_%PLATFORM%"

echo ""
echo BUILDING
echo ""
echo tag: "%TAG%"

echo docker build --tag %TAG% -f .\%PLATFORM%\Dockerfile . >> docker.log
docker build --tag %TAG% -f .\%PLATFORM%\Dockerfile . >> docker.log
echo ""
echo ""
echo PUSHING
echo ""
echo docker push %TAG%
docker push %TAG%
echo docker push %TAG_LATEST%
docker image tag %TAG% %TAG_LATEST%
docker push %TAG_LATEST%



REM docker build --tag marcvs/build_putty_win-msys2-mingw32:2   -f .\windows\msys2-mingw32\Dockerfile .


exit 0

# For msys2-mingw32, basically... 
#  - run the docker build command until it fails
#  - run the latest image:
#    docker run -i <image id>
#  - run the commands by hand
#  - exit the container
#  - docker commit <container>
#  - docker tag <image> marcvs/build_putty_win-msys2-mingw32:2
#  - docker push marcvs/build_putty_win-msys2-mingw32:2
#  - docker rm <container>
#
#  Further reading: https://www.dataset.com/blog/create-docker-image/
