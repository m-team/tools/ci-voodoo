echo off

set VERSION=3
set NAMESPACE=marcvs/build
set NAME=oidc-agent

cp ../ci-tools/windows/win-ci-interface.sh .

REM # Debug run:     docker run --rm -i  marcvs/build_oidc-agent_win-msys2:3

set PLATFORM=win-msys2

echo ""
echo BUILDING

echo %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%
docker build --tag %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION% -f .\%PLATFORM%\Dockerfile . >> docker.log
echo ""
echo ""
echo PUSHING
echo ""
echo %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%
docker push %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%

REM REPEAT THE ABOVE WITH DIFFERENT PLATFORM NAME

set PLATFORM=win-msys2-mingw64

echo ""
echo BUILDING

echo %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%
docker build --tag %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION% -f .\%PLATFORM%\Dockerfile . >> docker.log
echo ""
echo ""
echo PUSHING
echo ""
echo %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%
docker push %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%



REM docker build --tag marcvs/build_oidc-agent_win-msys2:2           -f .\windows\msys2\Dockerfile . >> docker-msys2.log
REM echo msys2-mingw64
REM docker build --tag marcvs/build_oidc-agent_win-msys2-mingw64:2   -f .\windows\msys2-mingw64\Dockerfile . >> docker-msys2-mingw64.log
REM
REM echo msys2-mingw32  # see below
REM echo docker build --tag marcvs/build_oidc-agent_win-msys2-mingw32:2   -f .\windows\msys2-mingw32\Dockerfile .
REM
REM # Debug run:     docker run --rm -i  marcvs/build_oidc-agent_win-msys2:2
REM # Debug compile: docker run --rm -i  marcvs/build_oidc-agent_win-msys2:2 powershell --branch dev_4.3.0
REM
REM echo ""
REM echo ""
REM echo PUSHING
REM echo ""
REM echo msys2
REM docker push marcvs/build_oidc-agent_win-msys2:2
REM echo ""
REM echo msys2-mingw64
REM docker push marcvs/build_oidc-agent_win-msys2-mingw64:2
REM #echo ""
REM #echo msys2-mingw32
REM #docker push marcvs/build_oidc-agent_win-msys2-mingw32:2
REM
REM exit 0
REM # For msys2-mingw32, basically...
REM #  - run the docker build command until it fails
REM #  - run the latest image
REM #  - run the commands by hand
REM #  - exit the container
REM #  - docker commit <container>
REM #  - docker tag <image> marcvs/build_oidc-agent_win-msys2-mingw32:2
REM #  - docker push marcvs/build_oidc-agent_win-msys2-mingw32:2
REM #  - docker rm <container>
REM #
REM #  Further reading: https://www.dataset.com/blog/create-docker-image/
