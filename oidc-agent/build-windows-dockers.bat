echo off

for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%" 
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
set "VERSION=%YY%%MM%%DD%"

set NAMESPACE=marcvs/build
set NAME=oidc-agent

cp ../ci-tools/windows/win-ci-interface.sh .

REM # Debug run:     docker run --rm -i  marcvs/build_oidc-agent_win-msys2:3


set PLATFORM=win-msys2

set "TAG=%NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%"
set "TAG_LATEST=%NAMESPACE%_%NAME%_%PLATFORM%:latest"
set "TAG_SHORT=%NAMESPACE%_%NAME%_%PLATFORM%"

echo ""
echo BUILDING %PLATFORM%
echo ""
echo tag: "%TAG%"

REM  echo docker build --tag %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION% -f .\%PLATFORM%\Dockerfile . >> docker.log
REM  docker build --tag %NAMESPACE%_%NAME%_%PLATFORM%:%VERSION% -f .\%PLATFORM%\Dockerfile . >> docker.log
echo docker build --tag %TAG% -f .\%PLATFORM%\Dockerfile . >> docker.log
docker build --tag %TAG% -f .\%PLATFORM%\Dockerfile . >> docker.log
echo ""
echo ""
echo PUSHING %TAG%
echo ""
echo docker push %TAG%
docker push %TAG%
echo docker push %TAG_LATEST%
docker image tag %TAG% %TAG_LATEST%
docker push %TAG_LATEST%


REM REPEAT THE ABOVE WITH DIFFERENT PLATFORM NAME

set PLATFORM=win-msys2-mingw64

set "TAG=%NAMESPACE%_%NAME%_%PLATFORM%:%VERSION%"
set "TAG_LATEST=%NAMESPACE%_%NAME%_%PLATFORM%:latest"
set "TAG_SHORT=%NAMESPACE%_%NAME%_%PLATFORM%"

echo ""
echo BUILDING
echo ""
echo tag: "%TAG%"

echo docker build --tag %TAG% -f .\%PLATFORM%\Dockerfile . >> docker.log
docker build --tag %TAG% -f .\%PLATFORM%\Dockerfile . >> docker.log
echo ""
echo ""
echo PUSHING
echo ""
echo docker push %TAG%
docker push %TAG%
echo docker push %TAG_LATEST%
docker image tag %TAG% %TAG_LATEST%
docker push %TAG_LATEST%




REM docker build --tag marcvs/build_oidc-agent_win-msys2:2           -f .\windows\msys2\Dockerfile . >> docker-msys2.log
REM echo msys2-mingw64
REM docker build --tag marcvs/build_oidc-agent_win-msys2-mingw64:2   -f .\windows\msys2-mingw64\Dockerfile . >> docker-msys2-mingw64.log
REM
REM echo msys2-mingw32  # see below
REM echo docker build --tag marcvs/build_oidc-agent_win-msys2-mingw32:2   -f .\windows\msys2-mingw32\Dockerfile .
REM
REM # Debug run:     docker run --rm -i  marcvs/build_oidc-agent_win-msys2:2
REM # Debug compile: docker run --rm -i  marcvs/build_oidc-agent_win-msys2:2 powershell --branch dev_4.3.0
REM
REM echo ""
REM echo ""
REM echo PUSHING
REM echo ""
REM echo msys2
REM docker push marcvs/build_oidc-agent_win-msys2:2
REM echo ""
REM echo msys2-mingw64
REM docker push marcvs/build_oidc-agent_win-msys2-mingw64:2
REM #echo ""
REM #echo msys2-mingw32
REM #docker push marcvs/build_oidc-agent_win-msys2-mingw32:2
REM
REM exit 0
REM # For msys2-mingw32, basically...
REM #  - run the docker build command until it fails
REM #  - run the latest image:          docker run -i <image id>
REM #  - run the commands by hand
REM #  - exit the container
REM #  - docker container ls -a | head -n 2
REM #  - docker commit <container>      @ this will take a while
REM #  - docker image ls -a | head -n 2
REM #  - docker tag <image> <tag (marcvs/build_oidc-agent_win-msys2-mingw32:2)>
REM #  - docker push <tag (marcvs/build_oidc-agent_win-msys2-mingw32:2)>
REM #  - docker rm <container>
REM #
REM #  Further reading: https://www.dataset.com/blog/create-docker-image/
