#!/bin/bash

# This script is called within the docker image for windows builds.
# As a workaround, it will be called "powershell" inside the VM.

set -x
set -e

CI_BUILD="true"
MANUAL_BUILD_PATH=""
MANUAL_BUILD_GIT="https://github.com/indigo-dc/oidc-agent.git"
MANUAL_BUILD_BRANCH="master"
MANUAL_BUILD_NAME="oidc-agent"
MANUAL_BUILD_TARGET=""

usage() {
    echo "
    -h|--help)
    --git)   
    --branch) 
    --name)    
    --target) (make target)
    "
}

while [ $# -gt 0 ]; do
    case "$1" in
    -h|--help)          usage           ; exit 0                            ;;
    --git)              CI_BUILD="false"; MANUAL_BUILD_GIT=$2       ; shift ;;
    --branch)           CI_BUILD="false"; MANUAL_BUILD_BRANCH=$2    ; shift ;;
    --name)             CI_BUILD="false"; MANUAL_BUILD_NAME=$2      ; shift ;;
    --target)           CI_BUILD="false"; MANUAL_BUILD_TARGET=$2    ; shift ;;
    esac
    shift
done

debug_info(){
    echo -e "\nI am the build script called like:"
    echo $0 $@
    #powershell -NoProfile -NoLogo -InputFormat text -OutputFormat text -NonInteractive -ExecutionPolicy Bypass -Command -

    echo "My original workdir:"
    pwd
    ls -la

    echo "MSYSTEM: $MSYSTEM"

    echo "CI_PROJECT_DIR: $CI_PROJECT_DIR"
    ls -ls $CI_PROJECT_DIR
}

ci_build(){
    echo -e "\nNow building; changing to $CI_PROJECT_DIR"
    cd $CI_PROJECT_DIR

    case "$MSYSTEM" in
        MSYS)   
            export PATH=$PATH:/mingw64/bin/
            make win_installer
        ;;
        MINGW64)
            export PATH=$PATH:/mingw64/bin/
            make
        ;;
        MINGW32)
            export PATH=$PATH:/mingw32/bin/
            make
        ;;
    esac


    echo -e "\nOUTPUT:bin"
    tree bin
    echo -e "\nOUTPUT:lib/api"
    tree lib/api
}

manual_build(){
    test -z $MANUAL_BUILD_PATH || {
        test -e $MANUAL_BUILD_PATH || mkdir -p $MANUAL_BUILD_PATH
        cd $MANUAL_BUILD_PATH
    }
    git clone $MANUAL_BUILD_GIT -b $MANUAL_BUILD_BRANCH $MANUAL_BUILD_NAME
    cd $MANUAL_BUILD_NAME

    echo "GIT INFO:"
    git status

    case "$MSYSTEM" in
        MINGW64)
            export PATH=$PATH:/mingw64/bin/
        ;;
        MINGW32)
            export PATH=$PATH:/mingw32/bin/
        ;;
    esac

    make $MANUAL_BUILD_TARGET

    echo -e "\nOUTPUT:bin"
    tree bin
    echo -e "\nOUTPUT:lib/api"
    tree lib/api
}


debug_info

[ "x$CI_BUILD" == "xtrue" ] && {
    ci_build
    exit 0
}
[ "x$CI_BUILD" == "xfalse" ] && {
    manual_build
    exit 0
}

