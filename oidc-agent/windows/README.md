This documents the windows and docker issues. The images built here are:
```
marcvs/build_oidc-agent_win-msys2:1
marcvs/build_oidc-agent_win-msys2-mingw64:1
marcvs/build_oidc-agent_win-msys2-mingw32:1
```

# Install Docker on Windows:

```
Install-Module -Name DockerMsftProvider -Repository PSGallery -Force
Install-Package -Name docker -ProviderName DockerMsftProvider
Restart-Computer -Force
```

# Build the image:

Generally, I'm following the [MSYS2 in CI](https://www.msys2.org/docs/ci/)
docs.

To build oidc-agent with the dockers built here use:
```
docker build --tag marcvs/build_oidc-agent_win-msys2:1           -f .\windows\msys2\Dockerfile .
docker build --tag marcvs/build_oidc-agent_win-msys2-mingw64:1   -f .\windows\msys2-mingw64\Dockerfile .
```


# Use an image (e.g. for debugging):
```
docker run --rm -i -v $PWD\:c:/msys64/home/build  marcvs/build_oidc-agent_win-msys2:1                      
```

## Run a compilation:
```
docker run --rm -i -v $PWD\:c:/msys64/home/build  marcvs/build_oidc-agent_win-msys2:1                      
git clone https://github.com/indigo-dc/oidc-agent
cd oidc-agent
git checkout <branch_name>
make
```

# The bad and the ugly

For 32 bit builds, things are heavily broken within windows and docker:
1. Windows nanocore [does not support
   32-bit](https://stackoverflow.com/a/64112817/3201632), so we have to
   use the (~2GB larger) servercore image.
1. Further, it seems like msys2-i686 (i.e. the 32-bit variant) is not
   further developed, so we use the latest version [from
   here](https://repo.msys2.org/distrib/i686/)
1. Then windows can't unpack tar.xz files, so I repackaged it to tar.gz an
   placed it on repo.data.kit.edu
  [http://repo.data.kit.edu/tools/msys2-base-i686-20210705.tar.gz](http://repo.data.kit.edu/tools/msys2-base-i686-20210705.tar.gz)
1. Finally, windows-docker has the [HCSSCHIM
   bug](https://dockerquestions.com/2021/10/26/hcsshimsystemcreateprocess-error-on-an-amazon-aws-gitlab-runner-running-windows/)

```
docker build --tag marcvs/build_oidc-agent_win-msys2-mingw32:1   -f .\windows\msys2-mingw32\Dockerfile .
```
Essentially, this means you can't build the image with `docker build`. It
works manually, though. I.e. copy-paste the contents of the
Dockerfile into the runing container:

- run the docker build command until it fails
- run the hash of the latest (or 2nd latest...) image
- run the commands from the [Dockerfile](msys-mingw32/Dockerfile) by hand
- exit the container
- `docker commit <container>`
- `docker tag <image> marcvs/build_oidc-agent_win-msys2-mingw32:1`
- `docker push marcvs/build_oidc-agent_win-msys2-mingw32:1`
- maybe also `docker rm <container>`


# Upload images to registry

```
docker login
docker push marcvs/build_oidc-agent_win-msys2:1
docker push marcvs/build_oidc-agent_win-msys2-mingw64:1
docker push marcvs/build_oidc-agent_win-msys2-mingw32:1
```
