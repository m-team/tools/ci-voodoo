#!/bin/bash

ORIG_EXEC=$0
ORIG_ARGS=$@

RUN="false"
BASE=$(cd "`dirname $0`" 2>/dev/null && pwd)
. ${BASE}/common-functions.sh

TARGET_REPO="/devel"
ALL_SUPPORTED_RPM_DISTROS="false"
ALL_SUPPORTED_DEB_DISTROS="false"
ALL_TYPE=""


while [ $# -gt 0 ]; do
    case "$1" in
    -t|--target-repo)   TARGET_REPO=$2;                 shift;;
    -d|--distro)        DISTRO=$2;                      shift;;
    -r|--release)       RELEASE=$2;                     shift;;
    --rpm)              ALL_SUPPORTED_RPM_DISTROS="true"
                        ALL_TYPE="rpm"                       ;;
    --deb)              ALL_SUPPORTED_DEB_DISTROS="true"
                        ALL_TYPE="deb"                       ;;
    -f|--file)          FILE_TO_CHECK=$2;               shift;;
    -c|--commit_sha)    COMMIT_SHA=$2;                  shift;;
    --run)              RUN="true"                           ;;
    *)                  FILE_TO_CHECK=$1;                    ;;
    esac
    shift
done

[ "x${RUN}" == "xfalse" ] && {
    update_from_git
}

[ -z ${DISTRO} ] && {
    [ -z ${ALL_TYPE} ] && {
        echo "ERROR: DISTRO is not set."
        exit 1
    }
}
[ -z ${RELEASE} ] && {
    [ -z ${ALL_TYPE} ] && {
        echo "ERROR: RELEASE is not set."
        exit 11
    }
}
[ -z ${TARGET_REPO} ] && {
    echo "ERROR: TARGET_REPO is not set."
    exit 111
}
# make sure TARGET_REPO starts with "/"
TARGET_REPO=$(sanitize_slashes ${TARGET_REPO})

REPO_BASE="/var/www${TARGET_REPO}"
[[ -z ${ALL_TYPE} ]] && {
    COMMIT_SHA_DB="/var/tmp/repodata${TARGET_REPO}-${DISTRO}-${RELEASE}.sha"
}
[[ -z ${ALL_TYPE} ]] || {
    COMMIT_SHA_DB="/var/tmp/repodata${TARGET_REPO}-goreleaser.sha"
}

[ -d ${REPO_BASE}/${DISTRO}/${RELEASE} ] || {
    echo "${REPO_BASE}/${DISTRO}/${RELEASE} does not exist!!!"
    exit 2
}

#echo "grep -q ${COMMIT_SHA} ${COMMIT_SHA_DB} "
[ "x${TARGET_REPO}" == "x/devel" ] || {
    [ -e ${REPO_BASE}/${DISTRO}/${RELEASE}/${FILE_TO_CHECK} ] && {
        grep -q ${COMMIT_SHA} ${COMMIT_SHA_DB} && {
            echo "File was already published. (0)"
            echo "Found it in commit hash db"
            echo "    Not overwriting"
            cat - > /dev/null
            exit 0
        }
        grep -q ${COMMIT_SHA} ${COMMIT_SHA_DB} || {
            echo "File was already published with a different commit hash."
            echo "    Abort (4)"
            exit 4
        }
    }
}

# store commit hash
grep -q ${COMMIT_SHA} ${COMMIT_SHA_DB} || {
    echo "        New commit hash. Storing: ${COMMIT_SHA} => ${COMMIT_SHA_DB}"
    echo ${COMMIT_SHA} >> ${COMMIT_SHA_DB}
    chmod 664 ${COMMIT_SHA_DB} || true
}

# store file
# If DISTRO is defined, we use the classic approach:
[ -z "${DISTRO}" ] || {
    cat - > ${REPO_BASE}/${DISTRO}/${RELEASE}/${FILE_TO_CHECK}
    ls -l ${REPO_BASE}/${DISTRO}/${RELEASE}/${FILE_TO_CHECK}
    exit 0
}
# If DISTRO is not defined, we assume goreleaser
[ -z "${DISTRO}" ] && {
    echo -e "\n----- ${FILE_TO_CHECK} ------"
    cat - > ${REPO_BASE}/goreleaser/${ALL_TYPE}/${FILE_TO_CHECK}
    echo "    file placed"
    for DISTRO in $(get_supported_distros ${ALL_TYPE}); do
        for RELEASE in $(get_supported_distros ${DISTRO}); do
            (
                printf "    going to link%28s" "${DISTRO}/${RELEASE}"
                cd ${REPO_BASE}/${DISTRO}/${RELEASE}
                [ -L ${FILE_TO_CHECK} ] && {
                    printf "  Link existed: in %28s %s" "${DISTRO}/${RELEASE}" " => updating link"
                    rm -f ${FILE_TO_CHECK}
                }
                [ -e ${FILE_TO_CHECK} ] && {
                    printf "  FILE EXISTED: in %28s %s" "${DISTRO}/${RELEASE}" "=> replacing"
                    rm -f ${FILE_TO_CHECK}
                }
                # echo "    linking file: ln -s ../../goreleaser/${ALL_TYPE}/${FILE_TO_CHECK} ."
                ln -s ../../goreleaser/${ALL_TYPE}/${FILE_TO_CHECK} .
                echo "  done"
            )
        done
    done
}
