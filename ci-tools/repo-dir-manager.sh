#!/bin/bash

BASE=$(cd "`dirname $0`" 2>/dev/null && pwd)
. ${BASE}/common-functions.sh

REPO_BASE_DIR=/var/www
REPO_DEVEL_DIR=/var/www/devel
REPO_PREREL_DIR=/var/www/prerel
REPO_PREPROD_DIR=/var/www/preprod
REPO_PROD_DIR=/var/www/prod

BASE_USER=build
BASE_GRUP=root
BASE_PERM=755

# These are premissions for access from cicd pipelines
STAG_USER=cicd
STAG_GRUP=root
STAG_PERM=755
STAG_PERM_FILES=644

[ $UID -ne 0 ] && {
    echo "This script needs to be run as root:"
    echo "    sudo $0$@"
    exit 1
}
DISTS=""
for DISTRO in $(get_supported_distros); do 
    for RELEASE in $(get_supported_distros ${DISTRO}); do
        DISTS="${DISTS} $DISTRO/${RELEASE}"
    done
done

chown root:root     $REPO_BASE_DIR/*
chmod 644           $REPO_BASE_DIR/*
chmod 755           $REPO_BASE_DIR/tools
chmod 755           $REPO_BASE_DIR/prod

for DIR in $REPO_BASE_DIR; do 
    for DIST in $DISTS; do 
        test -d $DIR/$DIST || {
            echo "creating $REPO_BASE_DIR/$DIST"
            mkdir -p $DIR/$DIST
        }

        SYMLINKS=$(get_symlinks $DIST)
        for LINK in $SYMLINKS; do
            [ -L $DIR/`dirname $DIST`/$LINK ] || {
                echo "Creating symlink: $DIR/`dirname $DIST` $LINK"
                (
                    cd $DIR/`dirname $DIST`
                    ln -s `basename $DIST` $LINK
                )
            }
        done

        chown -R $BASE_USER:$BASE_GRUP  `dirname $DIR/$DIST`
        chmod -R $BASE_PERM             `dirname $DIR/$DIST`

        chown -R $BASE_USER:$BASE_GRUP  $DIR/$DIST
        chmod -R $BASE_PERM             $DIR/$DIST

    done
    chown $BASE_USER:$BASE_GRUP  $DIR/*gpg $DIR/*repo $DIR/index.html
done

# devel prerel preprod
for DIR in $REPO_DEVEL_DIR $REPO_PREREL_DIR $REPO_PREPROD_DIR; do 
    for DIST in $DISTS; do 
        test -d $DIR/$DIST || {
            echo "creating $DIR/$DIST"
            mkdir -p $DIR/$DIST
        }

        SYMLINKS=$(get_symlinks $DIST)
        for LINK in $SYMLINKS; do
            [ -L $DIR/`dirname $DIST`/$LINK ] || {
                echo "Creating symlink: $DIR/`dirname $DIST` $LINK"
                (
                    cd $DIR/`dirname $DIST`
                    ln -s `basename $DIST` $LINK
                )
            }
            #chown $STAG_USER:$STAG_GRUP $DIR/`dirname $DIST`/$LINK
        done

        echo "chown -R $STAG_USER:$STAG_GRUP  $DIR/$DIST"
        chown -R $STAG_USER:$STAG_GRUP  $DIR/$DIST
        chmod -R $STAG_PERM             $DIR/$DIST
        NUM_ENTRIES=`ls $DIR/$DIST/ | wc -l`
        [ $NUM_ENTRIES -gt 0 ] && {
            chmod $STAG_PERM_FILES          $DIR/$DIST/*
        }
        [ -d $DIR/$DIST/repodata/ ] && {
            chmod $STAG_PERM                $DIR/$DIST/repodata/
        }

    done
    chown $STAG_USER:$STAG_GRUP  $DIR/*gpg
done
for DIR in $REPO_DEVEL_DIR $REPO_PREREL_DIR $REPO_PREPROD_DIR; do
    echo "chown $STAG_USER:$STAG_GRUP  $DIR"
    chown $STAG_USER:$STAG_GRUP  $DIR
    chmod $STAG_PERM             $DIR

    # goreleaser folders
    [[ -d ${DIR}/goreleaser/rpm ]] || {
        mkdir -p ${DIR}/goreleaser/rpm
    }
    [[ -d ${DIR}/goreleaser/deb ]] || {
        mkdir -p ${DIR}/goreleaser/deb
    }
    echo "chown -R $STAG_USER:root ${DIR}/goreleaser"
    chown -R $STAG_USER:root ${DIR}/goreleaser
done

# Prod
for DIR in $REPO_PROD_DIR; do
    echo "chown $BASE_USER:$BASE_GRUP  $DIR"
    chown $BASE_USER:$BASE_GRUP  $DIR
    chmod $BASE_PERM             $DIR

    # goreleaser folders
    [[ -d ${DIR}/goreleaser/rpm ]] || {
        mkdir -p ${DIR}/goreleaser/rpm
    }
    [[ -d ${DIR}/goreleaser/deb ]] || {
        mkdir -p ${DIR}/goreleaser/deb
    }
    echo "chown -R $BASE_USER:root ${DIR}/goreleaser"
    chown -R $BASE_USER:root ${DIR}/goreleaser
done
#chown -R $BASE_USER:$BASE_GRUP  $REPO_BASE_DIR
#chmod -R $BASE_PERM             $REPO_BASE_DIR
