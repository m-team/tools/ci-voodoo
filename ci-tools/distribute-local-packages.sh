#!/bin/bash

ORIG_EXEC=$0
ORIG_ARGS=$@

RUN="false"
BASE=$(cd "`dirname $0`" 2>/dev/null && pwd)
. ${BASE}/common-functions.sh

TARGET_REPO="/devel"
WIN_DISTRO="windows"

LOCAL_PACKAGES="/tmp/package-upload"
LOCAL_PACKAGE_ARCHIVE_DIR="/var/tmp/distribute-local-packages-archive"
DEBUG_MODE=""

test -d ${LOCAL_PACKAGE_ARCHIVE_DIR} || {
    mkdir -p ${LOCAL_PACKAGE_ARCHIVE_DIR}/deb
}

while [ $# -gt 0 ]; do
    case "$1" in
    -t|--target-repo)   TARGET_REPO=$2;                 shift;;
    -w|--windows)       WIN_DIR=$2;                     shift;;
    --run)              RUN="true"                           ;;
    --debug)            DEBUG_MODE="true"                    ;;
    esac
    shift
done

[ "x${RUN}" == "xfalse" ] && {
    update_from_git
}

[ -z ${TARGET_REPO} ] && {
    echo "ERROR: TARGET_REPO is not set."
    exit 1
}
# make sure TARGET_REPO starts with "/"
TARGET_REPO=$(sanitize_slashes ${TARGET_REPO})

REPO_BASE="/var/www${TARGET_REPO}"

# make sure REPO_BASE does not end with "/"
[ "x${REPO_BASE: -1}" == "/" ] && {
    REPO_BASE=${REPO_BASE:0:-1}
}

for DISTRO in $(get_supported_distros "deb"); do
    RELEASES=$(get_supported_distros ${DISTRO})
    for RELEASE in $RELEASES; do
        echo "RELEASE: ${DISTRO} - ${RELEASE}"
        [ -d "${REPO_BASE}/${DISTRO}/${RELEASE}" ] || {
            mkdir -p "${REPO_BASE}/${DISTRO}/${RELEASE}"
        }
        # echo cp $LOCAL_PACKAGES/*.deb "${REPO_BASE}/${DISTRO}/${RELEASE}"
        [ -z ${DEBUG_MODE} ] && {
            cp -n $LOCAL_PACKAGES/*.deb "${LOCAL_PACKAGE_ARCHIVE_DIR}/deb/"
            (
                cd "${REPO_BASE}/${DISTRO}/${RELEASE}"
                for PACKAGE in $LOCAL_PACKAGES/*.deb; do
                    [ -e `basename ${PACKAGE}` ] || {
                        ln -s ${LOCAL_PACKAGE_ARCHIVE_DIR}/deb/`basename ${PACKAGE}` .
                    }
                done
            )
            # cp $LOCAL_PACKAGES/*.deb "${REPO_BASE}/${DISTRO}/${RELEASE}"
        }
    done
done

for DISTRO in $(get_supported_distros "rpm"); do
    RELEASES=$(get_supported_distros ${DISTRO})
    for RELEASE in $RELEASES; do
        echo "RELEASE: ${DISTRO} - ${RELEASE}"
        [ -d "${REPO_BASE}/${DISTRO}/${RELEASE}" ] || {
            mkdir -p "${REPO_BASE}/${DISTRO}/${RELEASE}"
        }
        # echo cp $LOCAL_PACKAGES/*.rpm "${REPO_BASE}/${DISTRO}/${RELEASE}"
        [ -z ${DEBUG_MODE} ] && {
            cp $LOCAL_PACKAGES/*.rpm "${REPO_BASE}/${DISTRO}/${RELEASE}"
        }
    done
done

echo cp $LOCAL_PACKAGES/*windows* "${REPO_BASE}/${WIN_DISTRO}"
[ -z ${DEBUG_MODE} ] || {
    [ -z ${DEBUG_MODE} ] && {
        cp $LOCAL_PACKAGES/*windows* "${REPO_BASE}/${WIN_DISTRO}"
    }
}
