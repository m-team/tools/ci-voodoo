update_from_git() {
    (cd ${BASE}; git pull > /dev/null 2>&1)
    #echo "calling $ORIG_EXEC $ORIG_ARGS --run"
    $ORIG_EXEC $ORIG_ARGS --run
    exit $?
}

sanitize_slashes() {
    VALUE=$1
    [ "x${VALUE:0:1}" != "x/" ] && {
        VALUE="/${VALUE}"
    }
    # make sure VALUE does not end with "/"
    [ "x${VALUE: -1}" == "/" ] && {
        VALUE=${VALUE:0:-1}
    }
    echo ${VALUE}
}

get_all_projects() {
    echo "mytoken mytoken-server oidc-agent pam-ssh-oidc oidc-agent motley-cue oinit mustach webssh-oidc nginx-location-includer"
}

get_distro_type () {
    case "$1" in
        debian)         echo "deb" ;;
        ubuntu)         echo "deb" ;;
        centos)         echo "rpm" ;;
        fedora)         echo "rpm" ;;
        rockylinux)     echo "rpm" ;;
        almalinux)      echo "rpm" ;;
        opensuse)       echo "rpm" ;;
        rpm)            echo "rpm" ;;
        deb)            echo "deb" ;;
        windows)        echo "win" ;;
    esac
}

get_packages_for_project () {
    DISTRO_TYPE=""
    [ -z $2 ] || {
        DISTRO_TYPE=$(get_distro_type $2)
        # echo "DISTRO TYPE: ${DISTRO_TYPE}"
    }
        
    case "$1" in
        mytoken)        echo "mytoken" ;;
        mytoken-server) echo "mytoken-server mytoken-server-migratedb mytoken-server-setup mytoken-notifier-server" ;;
        orpheus)        echo "orpheus" ;;
        pam-ssh-oidc)   echo "pam-ssh-oidc pam-ssh-oidc-autoconfig" ;;
        oidc-agent)     
            case "${DISTRO_TYPE}" in
                rpm)
                    echo "oidc-agent oidc-agent-cli oidc-agent-desktop oidc-agent-libs oidc-agent-devel";;
                deb)
                    echo "oidc-agent oidc-agent-cli oidc-agent-desktop liboidc-agent5 liboidc-agent-dev";;
                # *)
                #     echo "oidc-agent oidc-agent-cli oidc-agent-desktop liboidc-agent5 liboidc-agent-dev";;
            esac
            ;;
        oinit)          echo "oinit oinit-ca oinit-openssh";;
        mustach)        echo "mustach-lib-cjson mustach-lib-jansson mustach-lib-json-c";;
        # webssh-oidc)    echo "webssh-oidc";;
        # nginx-location-includer) echo "nginx-location-includer";;
        *)              echo "$1" ;;
    esac
}

get_supported_distros () {
    case "$1" in
        debian)         echo "bookworm bullseye trixie"          ;;
        ubuntu)         echo "bionic focal jammy noble"         ;;
        centos)         echo "8"                               ;;
        fedora)         echo "38 39 40"                                 ;;
        rockylinux)     echo "8 9"                                    ;;
        almalinux)      echo "8 9"                                    ;;
        opensuse)       echo "15.5 15.6 tumbleweed"                     ;;
        rpm)            echo "centos fedora rockylinux opensuse almalinux"     ;;
        deb)            echo "debian ubuntu "                           ;;
        windows)        echo "oidc-agent"                               ;;
        *)              echo "debian ubuntu centos fedora rockylinux opensuse almalinux windows" ;;
    esac
}

get_supported_distros_including_deprecated_ones () {
    case "$1" in
        debian)         echo "buster bookworm bullseye trixie"          ;;
        ubuntu)         echo "xenial bionic focal jammy noble lunar"         ;;
        centos)         echo "7 8 stream"                               ;;
        fedora)         echo "34 36 37 38 39 40"                                 ;;
        rockylinux)     echo "8 8.5 8.9 9"                                    ;;
        almalinux)      echo "8 8.7 8.9 9"                                    ;;
        opensuse)       echo "15.2 15.3 15.4 15.5 15.6 tumbleweed"                     ;;
        rpm)            echo "centos fedora rockylinux opensuse almalinux"     ;;
        deb)            echo "debian ubuntu "                           ;;
        windows)        echo "oidc-agent"                               ;;
        *)              echo "debian ubuntu centos fedora rockylinux opensuse almalinux" ;;
    esac
}

get_supported_distros_and_releases () {
    DISTROS="$(get_supported_distros deb) $(get_supported_distros rpm)"
    for DISTRO in ${DISTROS}; do
        RELEASES=$(get_supported_distros $DISTRO)
        for RELEASE in ${RELEASES}; do
            echo "${DISTRO}-${RELEASE}"
        done
    done
}

get_symlinks () {
    case "$1" in
        centos/7)       echo "centos7"                                  ;;
        centos/8)       echo "centos8"                                  ;;
        centos/stream)  echo "centos-stream"                            ;;
        debian/bullseye)echo "11 oldstable"                             ;;
        debian/bookworm)echo "12 stable"                                ;;
        debian/trixie)  echo "13 testing"                               ;;
        ubuntu/bionic)  echo "18.04"                                    ;;
        ubuntu/focal)   echo "20.04"                                    ;;
        ubuntu/jammy)   echo "22.04"                                    ;;
        ubuntu/lunar)   echo "23.04"                                    ;;
        ubuntu/noble)   echo "24.04"                                    ;;
    esac
}
