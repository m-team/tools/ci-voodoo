#!/bin/bash

BASE=$(cd "`dirname $0`" 2>/dev/null && pwd)
. ${BASE}/../ci-tools/common-functions.sh

DOCKER_OPTIONS="--security-opt apparmor=unconfined"
VERBOSE=""
# VERSION=`date +%y%m%d-%H%M`
VERSION=`date +%y%m%d`
PREF=marcvs/build
PARALLEL_WORKERS=4
DO_STATS="false"
SINGLE_DIST=""
DO_PUSH="true"
FORCE="no"

usage() {
    echo " -c|--clean)         Don't use docker cache (--no-cache)"
}

while [ $# -gt 0 ]; do
    case "$1" in
    -h|--help)          usage        exit 0                          ;;
    -c|--clean)         DOCKER_OPTIONS="${DOCKER_OPTIONS} --no-cache";       ;;
    -v|-d)              VERBOSE="True"                               ;;
    -f|--force)         FORCE="yes"                                  ;;
    --version)          VERSION=$2;                             shift;;
    --name)             NAME=$2;                                shift;;
    --status)           DO_STATS="true"                              ;;
    --prefix|--pref)    PREF=$2;                                shift;;
    --distro)           SINGLE_DIST=$2;                         shift;;
    --nopush|no-push)   DO_PUSH="false";                        shift;;
    esac
    shift
done

[ "x${DO_STATS}" == "xtrue" ] && {
    while true; do 
        builders=`ps auxww|grep -v grep|grep docker\ build| wc -l`
        pushers=`ps auxww|grep -v grep|grep docker\ push | wc -l`
        total=`ps auxww|grep -v grep|grep docker\  | wc -l`

        echo "[b:${builders}|p:${pushers}|t:${total}]"
        sleep 10
    done
}

LOGS="logs"
test -d $LOGS || mkdir $LOGS
STATUS="status"
test -d $STATUS || mkdir $STATUS

DISTS=$(get_supported_distros_and_releases)
[ -z ${SINGLE_DIST} ] || {
    DISTS=${SINGLE_DIST}
}

# ACTIVE_WORKERS=0
#
# for WORKER in `seq 1 ${PARALLEL_WORKERS}`; do
#     ACTIVE_WORKERS=$((ACTIVE_WORKERS+1))
#     (
for DIST in $DISTS; do
    LOG="${LOGS}/${DIST}.log"
    STAT="${STATUS}/${DIST}"

    DISTRO=`echo $DIST | awk -F- '{ print $1 }'`
    RELEASE=`echo $DIST | awk -F- '{ print $2 }'`

    DFILE=$DISTRO/$RELEASE/Dockerfile
    TEMPLATE=Dockerfile-${DISTRO}.template
    TAG="${PREF}_${NAME}_${DIST}:${VERSION}"
    TAG_LATEST="${PREF}_${NAME}_${DIST}:latest"
    TAG_SHORT="${PREF}_${NAME}_${DIST}"


    [ -e ${STAT}.running ] && {
        echo "${DIST}: $DISTRO -- $RELEASE: ${TAG} => is built by another thread"
    }
    [ -e ${STAT}.running ] || {

        [[ "${FORCE}" == "yes" ]] && {
            rm ${STAT}.build
        }
        [ -e ${STAT}.build ] && {
            echo "${DIST}: $DISTRO -- $RELEASE: ${TAG} => was already built. Skipping"
        }
        [ -e ${STAT}.build ] || {
            echo -e "\n${DIST}: $DISTRO -- $RELEASE: ${TAG}"
            touch ${STAT}.running
            mkdir -p `dirname $DFILE`

            test -e $DFILE.template && {
                TEMPLATE=$DFILE.template
                echo "  Using alternative template file: $TEMPLATE"
            }
            cat $TEMPLATE \
                | sed s/@DISTRO@/$DISTRO/ \
                | sed s/@RELEASE@/$RELEASE/ \
            >  $DFILE
            date > $LOG

            [ -z  ${VERBOSE} ] || echo "   docker build ${DOCKER_OPTIONS} --tag $TAG   -f ${DFILE} ."
            docker build ${DOCKER_OPTIONS} --tag $TAG   -f ${DFILE} . >> $LOG 2>>$LOG
            # docker build ${DOCKER_OPTIONS} --tag $TAG   -f ${DFILE} . >> $LOG
            RV=$?
            [ $RV -eq 0 ] && {
                touch ${STAT}.build
                #docker build ${DOCKER_OPTIONS} --tag $TAG   -f ${DFILE} .
                [[ ${DO_PUSH} == "true" ]] && {
                    echo -e "  pushing ${DIST} as ${TAG}"
                    [ -z  ${VERBOSE} ] || echo "  docker push         $TAG"
                    docker push         $TAG  >> $LOG
                    # add + push "latest" tag
                    echo -e "  tagging ${DIST}    ${TAG} as LATEST"
                    docker image tag ${TAG} ${TAG_LATEST}
                    docker push ${TAG_LATEST} >> $LOG
                    [ $? -eq 0 ] && {
                        touch ${STAT}.push
                    }
                }
                [[ ${DO_PUSH} != "true" ]] && {
                    echo -e "  NOT pushing ${DIST} as ${TAG}"
                    echo "  To do this manually, run:"
                    echo "      docker push         $TAG"
                    echo "      docker image tag ${TAG} ${TAG_LATEST}"
                    echo "      docker push ${TAG_LATEST}"
                }

            }
            [ $RV -ne 0 ] && {
                echo -e "[Error|${DIST}: $DISTRO -- $RELEASE: ${TAG}]------------------------------------------"
                echo "ERROR, Return Value: $RV"
                cat $LOG
                echo -e "[/Error|${DIST}: $DISTRO -- $RELEASE: ${TAG}]-----------------------------------------"
                touch ${STAT}.error
            }
            [ $RV -eq 0 ] && {
                rm -f ${STAT}.error
            }
            rm ${STAT}.running
            echo ""
        }
    }
    [ -z  ${VERBOSE} ] || {
        echo "   docker images (docker image ls | grep $TAG_SHORT):"
        docker image ls | grep $TAG_SHORT | sed s/^/"    "/
    }
done
#     )&
#     echo "WORKER ${WORKER} done"
#     ACTIVE_WORKERS=$((ACTIVE_WORKERS-1))
# done

# while [ ${ACTIVE_WORKERS} -gt 0 ]; do
#     sleep 2
#     echo "ACTIVE: ${ACTIVE_WORKERS}"
# done

