#!/bin/bash

ORIG_EXEC=$0
ORIG_ARGS=$@

RUN="false"
BASE=$(cd "`dirname $0`" 2>/dev/null && cd .. ; pwd)
. ${BASE}/common-functions.sh
TEMPLATE_DIR=${BASE}/repo-info/templates
REPO_README_TEMPLATE=${TEMPLATE_DIR}/repo-readme.template
REPO_README_CSS=${TEMPLATE_DIR}/style_internal_css.html

REMOTE=cicd@repo.data.kit.edu
TARGET_REPO="/devel"

##########################################################################
# Function definitions
get_reponame() {
    case "$1" in
        "/")                echo "Production"                   ;;
        "/devel")           echo "Development"                  ;;
        "/prerel")          echo "Pre-Release"                  ;;
        "/preprod")         echo "Pre-Production"               ;;
    esac
}
get_repo_key_src() {
    case "$1" in
        "/")                echo "repo.data.kit.edu/repo-data-kit-edu-key.gpg"                  ;;
        *)                  echo "repo.data.kit.edu/devel/automatic-repo-data-kit-edu-key.gpg"  ;;
    esac
}
get_repo_key_dst() {
    case "$1" in
        "/")                echo "/etc/apt/trusted.gpg.d/kitrepo-archive.gpg"                   ;;
        *)                  echo "/etc/apt/trusted.gpg.d/auto-kitrepo-archive.gpg"              ;;
    esac
}
get_repo_key_file() {
    case "$1" in
        "/")                echo "${BASE}/repo-info/keys/repo-data-kit-edu-key.gpg"             ;;
        *)                  echo "${BASE}/repo-info/keys/automatic-repo-data-kit-edu-key.gpg"   ;;
    esac
}
get_key_type() {
    case "$1" in
        "/")                echo ""                             ;;
        *)                  echo "automatic-"                   ;;
    esac
}
generate_repo_index_html() {
    # Generate and copy to the right repo index.html
    REPO_README_HTML=$TMP/index.html
    REPONAME=$(get_reponame ${TARGET_REPO})
    REPO_KEY_SRC=$(get_repo_key_src ${TARGET_REPO})
    REPO_KEY_DST=$(get_repo_key_dst ${TARGET_REPO})

    [ -e $REPO_README_CSS ] || {
        echo "CSS template ${REPO_README_CSS} not found"
        exit 1
    }
    [ -e $REPO_README_TEMPLATE ] || {
        echo "Template ${REPO_README_TEMPLATE} not found"
    }
    [ -e $REPO_README_CSS ] && {
        cat $REPO_README_CSS > $REPO_README_HTML
    }
    [ -z $REPONAME ]     && {
        echo "REPONAME undefined"; exit 2
    }
    [ -z $REPO_KEY_SRC ] && {
        echo "REPO_KEY_SRC undefined"; exit 2
    }
    [ -z $REPO_KEY_DST ] && {
        echo "REPO_KEY_DST undefined"; exit 2
    }
    [ -z $TARGET_REPO ]  && {
        echo "TARGET_REPO undefined"; exit 2
    }

    REPONAME=$(get_reponame ${TARGET_REPO})
    cat $REPO_README_TEMPLATE \
            | sed s%@REPONAME@%$REPONAME%g \
            | sed s%@REPO_KEY_SRC@%$REPO_KEY_SRC%g \
            | sed s%@REPO_KEY_DST@%$REPO_KEY_DST%g \
            | sed s%@TARGET_REPO@%$TARGET_REPO%g \
        > $TMP/readme.md
    pandoc -f gfm -t html $TMP/readme.md >> $REPO_README_HTML
    #echo "scp $REPO_README_HTML $REMOTE:$REPO_BASE/index.html"
    scp $REPO_README_HTML $REMOTE:$REPO_BASE/index.html > /dev/null || echo "error with ssh"
    #scp $REPO_README_HTML $REMOTE:$REPO_BASE/index.html
}
generate_yum_repo_file() {
    d=`echo "data-kit-edu-${1}.repo" | sed s_/__`

    KEY_TYPE=$(get_key_type $TARGET_REPO)
    REPO_TYPE=${TARGET_REPO}
    #echo "KEY_TYPE: >${KEY_TYPE}<  REPO_TYPE: >$REPO_TYPE<"
    [ -e ${TEMPLATE_DIR}/${d}.template ] || {
        echo "${TEMPLATE_DIR}/${d}.template does not exist"
    }
    cat ${TEMPLATE_DIR}/${d}.template \
            | sed s%@REPO_TYPE@%${REPO_TYPE}/% \
            | sed s%@KEY_TYPE@%${KEY_TYPE}% \
            > $TMP/$d
    #echo scp $TMP/$d $REMOTE:$REPO_BASE/
    scp $TMP/$d $REMOTE:$REPO_BASE/ > /dev/null || echo "error copying REPO_FILE ${d}"
}
usage() {
    echo "-t|--target-repo"
    echo "--run           "
}
##########################################################################
# Parse args
while [ $# -gt 0 ]; do
    case "$1" in
    -t|--target-repo)   TARGET_REPO=$2;                 shift;;
    --run)              RUN="true"                           ;;
    -h|--help)          usage;           exit 0              ;;
    esac
    shift
done
##########################################################################
# sanitise
[ "x${RUN}" == "xfalse" ] && {
    update_from_git
}
[ -z ${TARGET_REPO} ] && {
    echo "ERROR: TARGET_REPO is not set."
    exit 1
}
TARGET_REPO=$(sanitize_slashes ${TARGET_REPO})
[ "x${TARGET_REPO}" == "x/" ] && {
    REMOTE=build@repo.data.kit.edu
}
##########################################################################
# Derived Variables;
REPO_BASE="/var/www${TARGET_REPO}"

##########################################################################
# Run
##########################################################################

TMP=`mktemp -d`
echo "TMP: ${TMP}"

echo "Creating repo index.html for $TARGET_REPO"
generate_repo_index_html

echo "Copying repo key"
KEY_FILE=$(get_repo_key_file $TARGET_REPO)
scp ${KEY_FILE} ${REMOTE}:${REPO_BASE}/ > /dev/null || echo "error copying key file ${KEY_FILE}"

echo "Generating yum/zypper based repo files"
for DISTRO in $(get_supported_distros "rpm"); do
    for RELEASE in $(get_supported_distros ${DISTRO}); do
        echo "    ${DISTRO} - ${RELEASE}"
        generate_yum_repo_file "${DISTRO}${RELEASE}"
    done
done
echo "done"
rm -rf $TMP
