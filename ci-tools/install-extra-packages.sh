#!/bin/bash

VERBOSE=0

while [[ "$#" -ge 1 ]]; do
    case "$1" in
        --verbose|-v)
            VERBOSE=1
            shift
            ;;
    esac
done

if [[ "$VERBOSE" -ne 0 ]]; then
    set -x
fi

echo "Installing extra packages for ${DISTRO}-${RELEASE}"

case "${DISTRO}" in
    debian|ubuntu)
        # APT
        if [[ -n "${MTEAM_CI_ADDITIONAL_PACKAGES_APT}" ]]; then
            apt-get -y install ${MTEAM_CI_ADDITIONAL_PACKAGES_APT} || {
                echo "Failed to install:"
                echo "    apt-get -y install ${MTEAM_CI_ADDITIONAL_PACKAGES_APT}"
            }
        fi
    ;;
    centos|almalinux|rockylinux|fedora)
        # YUM
        if [[ -n "${MTEAM_CI_ADDITIONAL_PACKAGES_YUM}" ]]; then
            yum -y install ${MTEAM_CI_ADDITIONAL_PACKAGES_YUM} || {
                echo "Failed to install:"
                echo "yum -y install ${MTEAM_CI_ADDITIONAL_PACKAGES_YUM}"
            }
        fi
    ;;
    opensuse)
        # ZYPPER
        if [[ -n "${MTEAM_CI_ADDITIONAL_PACKAGES_ZYPPER}" ]]; then
            zypper -n install ${MTEAM_CI_ADDITIONAL_PACKAGES_ZYPPER} || {
                echo "Failed to install:"
                echo "zypper -n install ${MTEAM_CI_ADDITIONAL_PACKAGES_ZYPPER}"
            }
        fi
    ;;
esac

