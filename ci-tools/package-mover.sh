#!/bin/bash

ORIG_EXEC=$0
ORIG_ARGS=$@

LOG=/tmp/package-mover-${UID}.log
rm -f $LOG

RUN="false"
BASE=$(cd "`dirname $0`" 2>/dev/null && pwd)
. ${BASE}/common-functions.sh

TARGET_REPO=""
SOURCE_REPO=""
VERSION="latest"
DISTROS=$(get_supported_distros_including_deprecated_ones)


REPO_BASE="/var/www"
STRICT="true"
OVERWRITE="false"
ACTION=""
NOOP="false"
SIGN="true"
FORCE_SIGN="false"
SIGN_REPO_COMMAND=${BASE}/sign-repo.sh
SIGN_KEY_NAME="auto"

usage() {
    echo "
    package-mover.sh <option> [<parameter>]

    Options:
    -s|--source-repo  ex: devel
    -t|--target-repo  ex: prerel
    -p|--project      ex: oidc-agent 
    -r|--version      not used, we alway handle the latest version
    -n|--noop         for use with copy: show what would happen, but don't do it
    -ns|--no-sign     don't sign the created repo (default: sign, except prod)
    --sign            sign even if repo is prod

    Actions: one of:
    -v|--view         just to show info
    -c|--copy         copy from source to target
    --prune|--delete)                   ACTION="prune"                   ;;
    "
}

while [ $# -gt 0 ]; do
    case "$1" in
    -s|--source-repo)                   SOURCE_REPO=$2;             shift;;
    -t|--target-repo)                   TARGET_REPO=$2;             shift;;
    -p|--project)                       PROJECT=$2;                 shift;;
    -r|--version)                       VERSION=$2;                 shift;;
    --permissive)                       STRICT="false";                  ;;
    --allow-mixed-package-versions)     STRICT="false";                  ;;
    --run)                              RUN="true"                       ;;
    --force-overwrite)                  OVERWRITE="true"                 ;;
    -v|--view)                          ACTION="view"                    ;;
    -c|--copy)                          ACTION="copy"                    ;;
    --prune|--delete)                   ACTION="prune"                   ;;
    -n|--noop)                          NOOP="true"                      ;;
    -ns|--no-sign)                      SIGN="false"                     ;;
    --sign)                             FORCE_SIGN="true"                ;;
    -h|--help)                          usage;   exit 0                  ;;
    *)                                  FILE_TO_CHECK=$1;                ;;
    esac
    shift
done

[ "x${RUN}" == "xfalse" ] && {
    update_from_git
}

SOURCE_REPO=$(sanitize_slashes ${SOURCE_REPO})
[ -z ${TARGET_REPO} ] || {
    TARGET_REPO=$(sanitize_slashes ${TARGET_REPO})
}

SOURCE_DIR="${REPO_BASE}${SOURCE_REPO}"
TARGET_DIR="${REPO_BASE}${TARGET_REPO}"

[ "x${TARGET_REPO}" == "x/" ] && {
    [ "x${FORCE_SIGN}" == "xtrue" ] || {
        [ x"${ACTION}" == "xcopy" ] && {
            echo "Prod-repo: not signing"
            SIGN="false"
        }
    }
    [ "x${FORCE_SIGN}" == "xtrue" ] && {
        echo "Prod-repo: signing enabled"
        SIGN="true"
        SIGN_KEY_NAME="prod"
    }
}

get_version () {
    # echo "    string: ${SOURCE_DIR}/${DISTRO}/${RELEASE}/${PACKAGE}[-_]" >> $LOG
    echo ${PKG_FILE} \
        | sed s%${SOURCE_DIR}/${DISTRO}/${RELEASE}/${PACKAGE}[-_]%% \
        | sed -E s%".(rpm|deb|exe)"%% \
        | sed -E s%".(i386|el[0-9].x86_64|fc[0-9][0-9].x86_64|x86_64|arm64|amd64|installer|all|el[0-9]|fc[0-9][0-9])"%% 
}
get_latest_version () {
    PKG_FILES=`find ${SOURCE_DIR}/${DISTRO}/${RELEASE}/ \
        | grep -E /${PACKAGE}[-_][0-9].*"\.(rpm|deb|exe)$"  \
        | grep -vE src.rpm`
    #echo "PKG_FILES: >>${PKG_FILES}<<"
    VERSIONS=""
    for PKG_FILE in ${PKG_FILES}; do
        # echo "PKG_FILE: ${PKG_FILE}" >> $LOG
        #echo "sed: s%${SOURCE_DIR}/${DISTRO}/${RELEASE}/${PACKAGE}[-_]%%"
        V=`get_version`
        # echo "    version: ${V}" >> $LOG
        VERSIONS=`echo -e "${VERSIONS} $V"`
    done
    echo "${VERSIONS} "| tr " " "\n" | sort -V | tail -n 1
}
get_all_but_latest_files () {
    PKG_FILES=`find ${SOURCE_DIR}/${DISTRO}/${RELEASE}/ \
        | grep -E ${PACKAGE}"(-debuginfo|-dbgsym)"?[-_][0-9].*"\.(rpm|deb|ddeb|exe|dsc)$"`
    #echo "PKG_FILES: >>${PKG_FILES}<<"
    VERSIONS=""
    for PKG_FILE in ${PKG_FILES}; do
        #echo "PKG_FILE: ${PKG_FILE}"
        #echo "sed: s%${SOURCE_DIR}/${DISTRO}/${RELEASE}/${PACKAGE}[-_]%%"
        LATEST=`get_latest_version`
        V=`get_version`
        [ "x$LATEST" != "x$V" ] && {
            #echo "not latest: $V"
            VERSIONS=`echo -e "${VERSIONS} $V"`
            PRUNABLE_PACKAGES=`echo -e "${PRUNABLE_PACKAGES} $PKG_FILE"`
        }
    done
    #echo "${VERSIONS} "| tr " " "\n" | sort -V | sed s/^$//
    echo "${PRUNABLE_PACKAGES} "| tr " " "\n"  
}
normalise_version() {
    echo $1 | sed -E s%"(\+ds|\+dsfg)"%%
}

# Loop over all to find out if versions are alright
GLOBAL_LATEST=""
FAILED_ON_RELEASE_VERSION_INCONSISTENT="false"
FAILED_ON_GLOBAL_VERSION_INCONSISTENT="false"
FAILED_ON_OVERWRITE="false"

echo ""
for DISTRO in ${DISTROS}; do
    for RELEASE in $(get_supported_distros ${DISTRO}); do
        [ -d ${SOURCE_DIR}/${DISTRO}/${RELEASE} ] || continue
        [ -L ${SOURCE_DIR}/${DISTRO}/${RELEASE} ] && continue
        #echo "${SOURCE_DIR} / ${DISTRO} / ${RELEASE}"

        # Get latest version per release
        DISTRO_RELEASE_LATEST=""

        PACKAGES=$(get_packages_for_project ${PROJECT} ${DISTRO})
        for PACKAGE in ${PACKAGES}; do 
            #echo "--- ${PACKAGE} ---"
            #get_latest_version
            LATEST=`get_latest_version`
            DISTRO_RELEASE_LATEST=`echo -e "${LATEST}\n${DISTRO_RELEASE_LATEST}" \
                | sort -V | tail -n 1`
        done
        #echo -en "\nDISTRO: $DISTRO - $RELEASE - "
        #echo "${PROJECT} - ${DISTRO_RELEASE_LATEST}"

        # Check for inconsistent versions inside RELEASE
        for PACKAGE in ${PACKAGES}; do 
            LATEST=`get_latest_version`
            [ -z ${LATEST} ] || {
                # Check if versions are consistent
                [ "x${DISTRO_RELEASE_LATEST}" != "x${LATEST}" ] && {
                    printf "%-65s %s %s\n" "WARNING: ${DISTRO}-${RELEASE}: ${PACKAGE}: version missing!"  "Actual: ${LATEST}"  "expected: ${DISTRO_RELEASE_LATEST}"
                    FAILED_ON_RELEASE_VERSION_INCONSISTENT="true"
                }
                # Check if target file exists (if we have a target repo)
                [ -z ${TARGET_REPO} ] || {
                    SRC_TMP=${SOURCE_DIR}/${DISTRO}/${RELEASE}
                    TGT_TMP=${TARGET_DIR}/${DISTRO}/${RELEASE}

                    MATCHING_SOURCE_FILES=`ls ${SRC_TMP}/${PACKAGE}_${LATEST}*deb ${SRC_TMP}/${PACKAGE}-${LATEST}*rpm ${SRC_TMP}/${PACKAGE}-${LATEST}*exe 2>/dev/null`
                    for SOURCE_FILE in ${MATCHING_SOURCE_FILES}; do
                        FILE=`echo ${SOURCE_FILE} | sed s%${SRC_TMP}/%%`
                        echo "FILE: ${DISTRO}-${RELEASE}-${PACKAGE}---${FILE}" >> $LOG

                        FILE_EXTENSION="${FILE##*.}"
                        [ -e ${TGT_TMP}/${FILE} ] && {
                            SRC_MD5=`md5sum ${SRC_TMP}/${FILE} | awk '{ print $1 }'`
                            TGT_MD5=`md5sum ${TGT_TMP}/${FILE} | awk '{ print $1 }'`
                            [ "x${SRC_MD5}" == "x${TGT_MD5}" ] || {
                                [ "x${OVERWRITE}" == "xtrue" ] || {
                                    # echo "WARNING: Different package with same version exists in target: ${FILE}"
                                    # echo "    ${SRC_MD5}!=${TGT_MD5}"
                                    [[ "${FILE_EXTENSION}" == "rpm" ]] && {
                                        # echo "    but it's likely just the rpm signature"
                                        x="123"
                                    }
                                    [[ "${FILE_EXTENSION}" == "rpm" ]] || {
                                        FAILED_ON_OVERWRITE="true"
                                    }
                                }
                            }
                        }
                    done
                }
            }
        done
        [ "x${STRICT}" == "xtrue" ] && {
            [ "x${FAILED_ON_RELEASE_VERSION_INCONSISTENT}" == "xtrue" ] && {
                echo -e "\nFailing. Use --allow-mixed-package-versions to override\n"
                echo -e "You WILL MAKE A MESS!\n"
                exit 1
            }
        }
        [ "x${FAILED_ON_OVERWRITE}" == "xtrue" ] && {
            echo -e "\nFailing: different files with same version exist in target"
            echo -e "Use --force-overwrite to override this\n"
            echo -e "You WILL MAKE A MESS!!\n"
            exit 2
        }

        # Check for globally inconsistent versions
        OLD_GLOBAL_LATEST=${GLOBAL_LATEST}
        GLOBAL_LATEST=`echo -e "${DISTRO_RELEASE_LATEST}\n${GLOBAL_LATEST}" \
            | sort -V | tail -n 1`
        [ -z ${OLD_GLOBAL_LATEST} ] || {
            NORMALISED_GLOBAL_LATEST=$(normalise_version ${GLOBAL_LATEST})
            NORMALISED_OLD_GLOBAL_LATEST=$(normalise_version ${OLD_GLOBAL_LATEST})
            [ "x${NORMALISED_GLOBAL_LATEST}" != "x${NORMALISED_OLD_GLOBAL_LATEST}" ] && {
                echo "There was an error with versions among different releases in your system."
                echo "Found  ${GLOBAL_LATEST}  and  ${OLD_GLOBAL_LATEST}  "
                FAILED_ON_GLOBAL_VERSION_INCONSISTENT="true"
            }
        }
        #echo "global latest: ${GLOBAL_LATEST}"
        [ "x${STRICT}" == "xtrue" ] && {
            [ "x${FAILED_ON_GLOBAL_VERSION_INCONSISTENT}" == "xtrue" ] && {
                echo -e "\nFailing global. Use --allow-mixed-package-versions to override"
                echo -e "You WILL MAKE A MESS!!!\n"
                exit 3
            }
        }
    done
done

#######################################################
# If we're here no version problems should exist
#######################################################
# Now we can run an action.

#[ "x$ACTION" == "x" ] && { #
#}

echo ""

[ "x$ACTION" == "xview" ] && { # show versions
    echo "These files and versions will be considered"
    for DISTRO in ${DISTROS}; do
        for RELEASE in $(get_supported_distros_including_deprecated_ones ${DISTRO}); do
            [ -d ${SOURCE_DIR}/${DISTRO}/${RELEASE} ] || continue
            [ -L ${SOURCE_DIR}/${DISTRO}/${RELEASE} ] && continue
            PACKAGES=$(get_packages_for_project ${PROJECT} ${DISTRO})
            for PACKAGE in ${PACKAGES}; do 
                LATEST=`get_latest_version`
                [ -z $LATEST ] || {
                    printf "%15s:%-13s -- %25s - %-8s\n" ${DISTRO} ${RELEASE} ${PACKAGE} ${LATEST}
                    #printf "    %-28s: %s\n" ${PACKAGE} ${LATEST}
                }
            done
        done
    done
}

[ "x$ACTION" == "xcopy" ] && { # Copy latest version from source to dest
    [ -z ${TARGET_REPO} ] && {
        echo "Specify target repo with -t|--target <repo name> option, please"
    }
    echo "Copying packages for ${PROJECT} from ${SOURCE_REPO} to ${TARGET_REPO}"
    for DISTRO in ${DISTROS}; do
        for RELEASE in $(get_supported_distros ${DISTRO}); do
            [ -d ${SOURCE_DIR}/${DISTRO}/${RELEASE} ] || continue
            [ -L ${SOURCE_DIR}/${DISTRO}/${RELEASE} ] && continue
            printf "[%s - %s]\n" ${DISTRO} ${RELEASE}
            PACKAGES=$(get_packages_for_project ${PROJECT} ${DISTRO})
            for PACKAGE in ${PACKAGES}; do 
                LATEST=`get_latest_version`
                [ -z $LATEST ] || {
                    printf "    %-28s: %s\n" ${PACKAGE} ${LATEST}

                    SRC_TMP=${SOURCE_DIR}/${DISTRO}/${RELEASE}
                    TGT_TMP=${TARGET_DIR}/${DISTRO}/${RELEASE}
                    MATCHING_SOURCE_FILES=`ls ${SRC_TMP}/${PACKAGE}_${LATEST}*deb ${SRC_TMP}/${PACKAGE}-${LATEST}*rpm ${SRC_TMP}/${PACKAGE}_${LATEST}*exe 2>/dev/null`
                    for SOURCE_FILE in ${MATCHING_SOURCE_FILES}; do
                        FILE=`echo ${SOURCE_FILE} | sed s%${SRC_TMP}/%%`
                        [ "x${NOOP}" == "xtrue" ] && {
                            echo "        cp ${SRC_TMP}/${FILE} ${TGT_TMP}/${FILE}"
                        }
                        [ "x${NOOP}" == "xtrue" ] || {
                            # [ -e ${TGT_TMP}/${FILE} ] &&
                            #     echo "WARNING: Overwriting target: ${DISTRO}/${RELEASE}/${FILE}"
                            cp ${SRC_TMP}/${FILE} ${TGT_TMP}/${FILE}
                            #chown ${REPO_OWNER}:u
                        }
                    done
                }
            done
            [ "x${NOOP}" == "xtrue" ] && {
                echo ""
            }
            [ "x${NOOP}" == "xtrue" ] || {
                ${SIGN_REPO_COMMAND} -t ${TARGET_REPO} -d ${DISTRO} -r ${RELEASE} -k ${SIGN_KEY_NAME}| sed s/^/"    "/
            }
        done
    done
}

[ "x$ACTION" == "xprune" ] && { # Delete all versions older that newest version
    [ -z ${PROJECT} ] && {
        PROJECTS=`get_all_projects`
    }
    [ -z ${PROJECT} ] || {
        PROJECTS=${PROJECT}
    }
    for PROJECT in ${PROJECTS}; do 
        echo "DELETING old packages for ${PROJECT} in ${SOURCE_REPO}"
        for DISTRO in ${DISTROS}; do
            for RELEASE in $(get_supported_distros_including_deprecated_ones ${DISTRO}); do
                [ -d ${SOURCE_DIR}/${DISTRO}/${RELEASE} ] || continue
                [ -L ${SOURCE_DIR}/${DISTRO}/${RELEASE} ] && continue
                printf "[%s - %s]\n" ${DISTRO} ${RELEASE}
                PACKAGES=$(get_packages_for_project ${PROJECT} ${DISTRO})
                for PACKAGE in ${PACKAGES}; do 
                    echo " pkg: ${PACKAGE}"
                    LATEST=`get_latest_version`
                    ALL_BUT_LATEST_FILES=`get_all_but_latest_files`
                    [ -z "${ALL_BUT_LATEST_FILES}" ] || {
                        echo -e "    $PACKAGE - LATEST: $LATEST"
                        for PKG in ${ALL_BUT_LATEST_FILES}; do
                            [ "x${NOOP}" == "xtrue" ] && {
                                echo "  NOOP  rm ${PKG}"
                            }
                            [ "x${NOOP}" == "xtrue" ] || {
                                echo "        rm ${PKG}"
                                rm ${PKG}
                            }
                        done
                    }
                done
            done
        done
    done
}
