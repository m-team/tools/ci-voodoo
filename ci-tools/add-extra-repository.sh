#!/bin/bash
# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

# Add an extra apt repository sources.list if the extra-repository argument is
# set. The extra_repository and extra_repository_key variables are filenames.
# The optional target-etc argument allows setting a different destination path
# to be able to update chroots.
# Passing the update flag will cause the script to call apt-get update at the
# end if the file was added.

SALSA_CI_EXTRA_REPOSITORY=""
SALSA_CI_EXTRA_REPOSITORY_KEY=""
TARGET_ETC="/etc"
VERBOSE=0

while [[ "$#" -ge 1 ]]; do
    case "$1" in
        --target-etc|-t)
            shift
            TARGET_ETC="$1"
            shift
            ;;
        --verbose|-v)
            VERBOSE=1
            shift
            ;;
    esac
done

if [[ "$VERBOSE" -ne 0 ]]; then
    set -x
fi


case "${DISTRO}" in
    debian|ubuntu)
        # REPOSOTORY_STRING and KEY or KEY_URL
        echo "Adding extra repo for ${DISTRO}-${RELEASE}"
        if [[ -n "${MTEAM_CI_EXTRA_REPOSITORY_STRING}" ]]; then
            mkdir -p "${TARGET_ETC}"/apt/sources.list.d/
            echo "${MTEAM_CI_EXTRA_REPOSITORY_STRING}" \
                > "${TARGET_ETC}"/apt/sources.list.d/extra_repository.list
            if [[ -n "${MTEAM_CI_EXTRA_REPOSITORY_KEY_URL}" ]]; then
                mkdir -p "${TARGET_ETC}"/apt/trusted.gpg.d/
                curl "${MTEAM_CI_EXTRA_REPOSITORY_KEY_URL}" \
                    | gpg --dearmor \
                    > "${TARGET_ETC}"/apt/trusted.gpg.d/extra_repository.gpg
            fi
            apt-get update
            # apt-get upgrade --assume-yes
        fi
    ;;
    centos|almalinux|rockylinux|fedora)
        # REPOSOTORY_URL_YUM
        echo "Adding extra repo for ${DISTRO}-${RELEASE}"
        if [[ -n "${MTEAM_CI_EXTRA_REPOSITORY_URL_YUM}" ]]; then
            echo "curling:  ${MTEAM_CI_EXTRA_REPOSITORY_URL_YUM}"
            wget "${MTEAM_CI_EXTRA_REPOSITORY_URL_YUM}"  -O "${TARGET_ETC}"/yum.repos.d/extra_repository.repo || {
                echo "no wget; using curl"
                curl --insecure "${MTEAM_CI_EXTRA_REPOSITORY_URL_YUM}"  > "${TARGET_ETC}"/yum.repos.d/extra_repository.repo
            }
            # yum -y clean || true
            # yum -y --refresh update
            echo "--- content of ${TARGET_ETC}/yum.repos.d/extra_repository.repo"
            cat ${TARGET_ETC}/yum.repos.d/extra_repository.repo
            echo "---"
        fi

    ;;
    opensuse)
        # REPOSOTORY_URL_ZYPPER
        echo "Adding extra repo for ${DISTRO}-${RELEASE}"
        if [[ -n "${MTEAM_CI_EXTRA_REPOSITORY_URL_ZYPPER}" ]]; then
            echo "curling: ${MTEAM_CI_EXTRA_REPOSITORY_URL_ZYPPER}"
            curl "${MTEAM_CI_EXTRA_REPOSITORY_URL_ZYPPER}" \
                > "${TARGET_ETC}"/zypp/repos.d/extra_repository.repo
            zypper --gpg-auto-import-keys refresh
        fi
    ;;
esac

