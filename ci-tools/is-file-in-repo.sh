#!/bin/bash

ORIG_EXEC=$0
ORIG_ARGS=$@

RUN="false"
BASE=$(cd "`dirname $0`" 2>/dev/null && pwd)
. ${BASE}/common-functions.sh

TARGET_REPO="/devel"


while [ $# -gt 0 ]; do
    case "$1" in
    -t|--target-repo)   TARGET_REPO=$2;                 shift;;
    -d|--distro)        DISTRO=$2;                      shift;;
    -r|--release)       RELEASE=$2;                     shift;;
    -f|--file)          FILE_TO_CHECK=$2;               shift;;
    -c|--commit_sha)    COMMIT_SHA=$2;                  shift;;
    --run)              RUN="true"                           ;;
    *)                  FILE_TO_CHECK=$1;                    ;;
    esac
    shift
done

[ "x${RUN}" == "xfalse" ] && {
    update_from_git
}
[ -z ${DISTRO} ] && {
    echo "ERROR: DISTRO is not set."
    exit 1
}
#[ "x${DISTRO}" == "xwindows" ] && {
#    exit 0
#}
[ -z ${RELEASE} ] && {
    echo "ERROR: RELEASE is not set."
    exit 1
}
[ -z ${TARGET_REPO} ] && {
    echo "ERROR: TARGET_REPO is not set."
    exit 1
}
# make sure TARGET_REPO starts with "/"
TARGET_REPO=$(sanitize_slashes ${TARGET_REPO})

[ -z ${FILE_TO_CHECK} ] && {
    echo "ERROR: FILE_TO_CHECK is not set."
    exit 1
}

REPO_BASE="/var/www${TARGET_REPO}"
COMMIT_SHA_DB="/var/tmp/repodata${TARGET_REPO}-${DISTRO}-${RELEASE}.sha"


[ -d ${REPO_BASE}/${DISTRO}/${RELEASE} ] || {
    echo "${REPO_BASE}/${DISTRO}/${RELEASE} does not exist!!!"
    exit 2
}

[ -e ${REPO_BASE}/${DISTRO}/${RELEASE}/${FILE_TO_CHECK} ] || {
    #echo "File >>${FILE_TO_CHECK}<< is not in repo"
    exit 0
}

[ -e ${REPO_BASE}/${DISTRO}/${RELEASE}/${FILE_TO_CHECK} ] && {
    echo "File >>${FILE_TO_CHECK}<< is already in repo ${DISTRO}/${RELEASE}. Checking commit hash DB"
    echo "ls -l ${FILE_TO_CHECK}"
    ls -l ${FILE_TO_CHECK}
    grep -q ${COMMIT_SHA} ${COMMIT_SHA_DB} && {
        echo "File was already published."
        echo "    No reason to fail"
        echo "    No need to continue."
    }
    grep -q ${COMMIT_SHA} ${COMMIT_SHA_DB} || {
        echo "File was already published from a different commit."
        echo "    (i.e. I did not find this commit hash in my db)"
        echo "    Fail"
        echo "    Cannot continue."
        echo "    You probably need to increase the version number. (Sorry)"
        exit 3
    }
}
