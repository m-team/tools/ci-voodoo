#!/bin/bash

# This script is called within the docker image for windows builds.
# As a workaround, it will be called "powershell" inside the VM.

set -e

echo $0 $@

GITLAB_CI_SCRIPT="/tmp/gitlab-ci-script.sh"
GITLAB_CI_SCRIPT_HEAD="/tmp/gitlab-ci-script-head"
GITLAB_CI_SCRIPT_BODY="/tmp/gitlab-ci-script-body"
CALL_SCRIPT_AFTER_CREATION="yes"
SCRIPT_TYPE="bash"

while [ $# -gt 0 ]; do
    case "$1" in
        -script)           GITLAB_CI_SCRIPT=$2;                shift;;
        -dont-call)        CALL_SCRIPT_AFTER_CREATION="no";         ;;
        -type)             SCRIPT_TYPE=$2;                     shift;;
    esac
    shift
done


[ x${SCRIPT_TYPE} == "xbash" ] && {
    echo "#!/bin/bash" > $GITLAB_CI_SCRIPT_HEAD
    echo "set -x >> $GITLAB_CI_SCRIPT_HEAD"
}

cat - > /tmp/raw-input.txt

# echo "----- raw-input.txt -------------------------"
# cat /tmp/raw-input.txt
# echo "------ end raw-ipnut.txt --------------------"

#echo " =---------- SSH KEYS ---------------="
#ls -la /tmp/ssh-private-keys  || echo "true"
#echo " =---------- SSH KEYS ---------------="

cat /tmp/raw-input.txt \
    | grep  'if(!$?) { Exit &{if($LASTEXITCODE) {$LASTEXITCODE} else {1}} }' -A 1000 \
    | grep -vE "(collapsed multi-line command|LASTEXITCODE)" \
    | grep -v '$ echo' \
    | grep -vE 'echo ".{1,9}\$' \
    >> $GITLAB_CI_SCRIPT_BODY

# Fix dangling closed brace in output:ќ
OPEN_BRACES=`tr -cd '{' < $GITLAB_CI_SCRIPT_BODY | wc -c`
CLOSED_BRACES=`tr -cd '}' < $GITLAB_CI_SCRIPT_BODY | wc -c`
BRACES_TO_CLOSE=$((OPEN_BRACES-CLOSED_BRACES))
[ $BRACES_TO_CLOSE -lt 0 ] && {
    for i in `seq $BRACES_TO_CLOSE -1`; do 
        echo "{" >> $GITLAB_CI_SCRIPT_HEAD
    done
}
[ $BRACES_TO_CLOSE -gt 0 ] && {
    for i in `seq 1 $BRACES_TO_CLOSE`; do 
        echo "}" >> $GITLAB_CI_SCRIPT_BODY
    done
}

# Finish off the gitlab script
echo "set +x >> $GITLAB_CI_SCRIPT_BODY"

cat $GITLAB_CI_SCRIPT_HEAD \
    $GITLAB_CI_SCRIPT_BODY \
    > $GITLAB_CI_SCRIPT

chmod 755 $GITLAB_CI_SCRIPT

ls -l $GITLAB_CI_SCRIPT
echo "WIN CI INTERFACE V3.5"
# echo "This is the content of the $GITLAB_CI_SCRIPT:"
# echo "-------- gitlab-ci-script.sh ----------------"
# cat $GITLAB_CI_SCRIPT
# echo "---- end gitlab-ci-script.sh ----------------"

#cp /tmp/raw-input.txt /tmp/ssh-


debug_info(){
    echo -e "\nI am the build script called like:"
    echo $0 $@
    #powershell -NoProfile -NoLogo -InputFormat text -OutputFormat text -NonInteractive -ExecutionPolicy Bypass -Command -

    echo "My original workdir:"
    pwd
    ls -la

    echo "MSYSTEM: $MSYSTEM"

    echo "CI_PROJECT_DIR: $CI_PROJECT_DIR"
    ls -ls $CI_PROJECT_DIR
}

env_prep(){
    case "$MSYSTEM" in
        MSYS)
            export PATH=$PATH:/mingw64/bin/
        ;;
        MINGW64)
            export PATH=$PATH:/mingw64/bin/
        ;;
        MINGW32)
            export PATH=$PATH:/mingw32/bin/
        ;;
    esac
}

ci_build(){
    echo -e "\nNow building; changing to $CI_PROJECT_DIR"
    cd $CI_PROJECT_DIR

    echo "Calling build script: $GITLAB_CI_SCRIPT"
    $GITLAB_CI_SCRIPT
}


env_prep
#debug_info

[ x${CALL_SCRIPT_AFTER_CREATION} == "xyes" ] && {
    ci_build
}

## Version: 14
