#!/bin/bash

ORIG_EXEC=$0
ORIG_ARGS=$@

RUN="false"
BASE=$(cd "`dirname $0`" 2>/dev/null && pwd)
. ${BASE}/common-functions.sh

### THESE MUST BE UPPERCASE
REPO_SIGN_KEY_PROD="ACDFB08FDC962044D87FF00B512839863D487A87"
REPO_SIGN_KEY_AUTO="F35D41344946F23D5D77AFD1E89B3987AEB6032B"
REPO_SIGN_KEY=""
TARGET_REPO="/devel"
PACKAGE_FILE_TO_SIGN=""

usage() {
    echo " sign-repo
    -t|--target-repo)       Target Repo (e.g. devel preprod (or / for prod))
    -d|--distro)            Distro (e.g. centos)
    -r|--release)           Release (e.g. 7)
    -k|--key)               empty or prod
    --rpmfile <package file>   rpm-only: just sign a specific package file
"
}

while [ $# -gt 0 ]; do
    case "$1" in
    -t|--target-repo)   TARGET_REPO=$2;                 shift;;
    -d|--distro)        DISTRO=$2;                      shift;;
    -r|--release)       RELEASE=$2;                     shift;;
    -k|--key)           REPO_SIGN_KEY=$2;               shift;;
    --run)              RUN="true"                           ;;
    --rpmfile)             PACKAGE_FILE_TO_SIGN=$2;        shift;;
    -h|--help)          usage;              exit 0           ;;
    esac
    shift
done

[[ -z "${REPO_SIGN_KEY}" ]] && {
    REPO_SIGN_KEY=${REPO_SIGN_KEY_AUTO}
}
[[ "${REPO_SIGN_KEY}" == "prod" ]] && {
    REPO_SIGN_KEY=${REPO_SIGN_KEY_PROD}
}
[[ "${REPO_SIGN_KEY}" == "auto" ]] && {
    REPO_SIGN_KEY=${REPO_SIGN_KEY_AUTO}
    case "${TARGET_REPO}" in 
        "/" | "/prod")
            REPO_SIGN_KEY=${REPO_SIGN_KEY_PROD}
        ;;
    esac
}

[ "x${RUN}" == "xfalse" ] && {
    update_from_git
}

[ -z ${DISTRO} ] && {
    echo "ERROR: DISTRO is not set."
    exit 1
}
[ -z ${RELEASE} ] && {
    echo "ERROR: RELEASE is not set."
    exit 1
}
[ -z ${TARGET_REPO} ] && {
    echo "ERROR: TARGET_REPO is not set."
    exit 1
}
# make sure TARGET_REPO starts with "/"
TARGET_REPO=$(sanitize_slashes ${TARGET_REPO})

REPO_BASE="/var/www${TARGET_REPO}"

deb-sign-repo() {
    #ssh ${REPO_USER}@${REPO_HOST} "
    cd ${REPO_BASE}/${DISTRO}/${RELEASE}

    #-- build Packages file
    apt-ftparchive packages . > Packages
    bzip2 -kf Packages
    #echo "Debian 'Packages' file created"

    #-- signed Release file
    apt-ftparchive release . > Release
    #echo "Debian 'Release' file created"

    test -e Release.gpg && rm Release.gpg
    gpg --yes -abs -u ${REPO_SIGN_KEY} -o Release.gpg Release
    #echo "Debian 'Release' file signed"
    echo "    done"
}
get-rpm-signature() {
    FILE=${1}
    # SIGN_RAW=$(rpm -qpi ${FILE} 2>/dev/null | grep Signature | cut -d: -f 2)
    SIGN_RAW=$(rpm -qpi ${FILE} 2>/dev/null | grep Signature)
    SIGN_PART_TESTSIG=$(echo "${SIGN_RAW}" | cut -d: -f 2)
    [[ ${SIGN_PART_TESTSIG} == " (none)" ]] && echo "false"
    [[ ${SIGN_PART_TESTSIG} == " (none)" ]] || echo "${SIGN_RAW}" | cut -d\  -f 15
}
rpm-sign-repo() {
    #ssh ${REPO_USER}@${REPO_HOST} "
    cd ${REPO_BASE}/${DISTRO}/${RELEASE}

    # FIXME: only re-sign those packages that we've brought (if that's
    # possible
    RV=0
    # Either sign all files
    [[ -z ${PACKAGE_FILE_TO_SIGN} ]] && {
        # rpmsign --delsign *rpm  > /dev/null 2>&1
        for FILE in *rpm; do
            RPM_SIGNATURE=$(get-rpm-signature ${FILE})
# ${REPO_SIGN_KEY}
            [[ ${RPM_SIGNATURE} == "false" ]] && {
                printf "%50s" "${FILE}..."
                echo -n " signing..."
                [[ -L ${FILE} ]] || {
                    echo -n "  actual file"
                    rpmsign --key-id ${REPO_SIGN_KEY} --addsign ${FILE} > /dev/null 2>&1
                }
                [[ -L ${FILE} ]] && {
                    ACTUAL_FILE=$(readlink -f ${FILE})
                    echo -n "  symlink: signing actual file"
                    rpmsign --key-id ${REPO_SIGN_KEY} --addsign ${ACTUAL_FILE} > /dev/null 2>&1
                }
                echo " done"
            }
            [[ ${RPM_SIGNATURE} == "false" ]] || {
                RPM_SIGN_KEY_FULL=$(gpg -k ${RPM_SIGNATURE} 2>/tmp/gpg-$$.log| grep -i ${RPM_SIGNATURE} | sed s"/ *//g")
                RV=$?
                [[ ${RV} == 0 ]] || {
                    printf "%50s" "${FILE}..."
                    echo -e \n "gpg had an error:"
                    cat /tmp/gpg-$$.log
                    RV=0
                }
                rm /tmp/gpg-$$.log
                [[ ${RPM_SIGN_KEY_FULL} != ${REPO_SIGN_KEY} ]] && {
                    printf "%50s" "${FILE}..."
                    # echo " signed with wrong key. "
                    # echo "                                       Have: >>>${RPM_SIGNATURE}<<< need >>>${REPO_SIGN_KEY}<<<"
                    echo -n "                                       signing..."
                    [[ -L ${FILE} ]] || {
                        echo -n "  actual file"
                        rpmsign --delsign ${FILE} > /dev/null 2>&1
                        rpmsign --key-id ${REPO_SIGN_KEY} --addsign ${FILE} > /dev/null 2>&1
                    }
                    [[ -L ${FILE} ]] && {
                        ACTUAL_FILE=$(readlink -f ${FILE})
                        echo -n "  symlink: signing actual file "
                        rpmsign --delsign ${ACTUAL_FILE} > /dev/null 2>&1
                        rpmsign --key-id ${REPO_SIGN_KEY} --addsign ${ACTUAL_FILE} > /dev/null 2>&1
                    }
                    echo "done (hopefully)"
                }
                # [[ ${RPM_SIGN_KEY_FULL} != ${REPO_SIGN_KEY} ]] || {
                #     echo " already signed ${RPM_SIGNATURE_FULL}"
                # }
            }
        done
    }
    # or sign individual file
    [[ -z ${PACKAGE_FILE_TO_SIGN} ]] || {
        echo "rpmsign --key-id ${REPO_SIGN_KEY} --addsign ${PACKAGE_FILE_TO_SIGN}"
        rpmsign --key-id ${REPO_SIGN_KEY} --addsign ${PACKAGE_FILE_TO_SIGN} > /dev/null 2>&1
        RV=$?
        echo "    Signed only signle file: ${PACKAGE_FILE_TO_SIGN}"
    }
    [[ ${RV} == 0 ]] || {
        echo "  -> Error signing rpms in ${PWD} <-"
    }

    echo -n "creating repo ..."
    /usr/bin/createrepo_c --database . > /dev/null
    #echo "Created repodata"

    rm -f repodata/repomd.xml.asc
    gpg --batch --detach-sign -u ${REPO_SIGN_KEY} --armor repodata/repomd.xml
    echo " done"
}


[ "x${DISTRO}" != "xwindows" ] && {
    printf "    Going to sign %28s" "${DISTRO}/${RELEASE}: "
}
# check if repo exists:
[ -d ${REPO_BASE}/${DISTRO}/${RELEASE} ] || {
    echo "${REPO_BASE}/${DISTRO}/${RELEASE} does not exist!!!"
    exit 2
}

case "${DISTRO}" in
  "debian")     deb-sign-repo     ;;
  "ubuntu")     deb-sign-repo     ;;
  "centos")     rpm-sign-repo     ;;
  "opensuse")   rpm-sign-repo     ;;
  "rockylinux") rpm-sign-repo     ;;
  "almalinux")  rpm-sign-repo     ;;
  "fedora")     rpm-sign-repo     ;;
  "windows")    echo "Windows: not signing";   exit 0            ;;
esac

