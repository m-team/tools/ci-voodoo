#!/bin/bash

ORIG_EXEC=$0
ORIG_ARGS=$@
ORIG_BASE=${0%/*}

RUN="false"
BASE=$(cd "`dirname $0`" 2>/dev/null && pwd)
. "${BASE}"/common-functions.sh

REPO_SIGN_KEY_PROD="ACDFB08FDC962044D87FF00B512839863D487A87"
REPO_SIGN_KEY="F35D41344946F23D5D77AFD1E89B3987AEB6032B"
TARGET_REPO="/devel"
    # ~/ci-voodoo/ci-tools/sign-repo.sh -t "${TARGET_REPO}" -d "${DISTRO}" -k "${REPO_SIGN_KEY}"

usage() {
    echo " sign-all-repos
    -t|--target-repo)   Target Repo (e.g. devel preprod (or / for prod))
    -d|--distro         Overwrite default list of distros (default: all)
    -k|--key)           empty or prod
"
}

while [ $# -gt 0 ]; do
    case "$1" in
    -t|--target-repo)   TARGET_REPO=$2;                 shift;;
    -d|--distro)        DISTROS=($2);                   shift;;
    -k|--key)           REPO_SIGN_KEY=$2;               shift;;
    --run)              RUN="true"                           ;;
    -h|--help)          usage;                    exit 0     ;;
    esac
    shift
done

[ "x${RUN}" == "xfalse" ] && {
    update_from_git
}
[ -z "${TARGET_REPO}" ] && {
    echo "ERROR: TARGET_REPO is not set."
    exit 1
}
# make sure TARGET_REPO starts with "/"
TARGET_REPO=$(sanitize_slashes "${TARGET_REPO}")

REPO_BASE="/var/www"${TARGET_REPO}""
for DISTRO in $(get_supported_distros); do 
    for RELEASE in $(get_supported_distros "${DISTRO}"); do
        #RELEASE=${rel:2}
        # echo -e "\n"${ORIG_BASE}"/sign-repo.sh -t "${TARGET_REPO}" -d "${DISTRO}" -r "${RELEASE}" -k "${REPO_SIGN_KEY}""
        echo -e "\n--- sign-all call sign-repo.sh for ${DISTRO} / ${RELEASE} ----"
        "${ORIG_BASE}"/sign-repo.sh -t "${TARGET_REPO}" -d "${DISTRO}" -r "${RELEASE}" -k "${REPO_SIGN_KEY}" --run
    done
done
