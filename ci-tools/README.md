## Test if signatures work

```sh
ssh -R /run/user/1000/gnupg/S.gpg-agent:/run/user/1000/gnupg/S.gpg-agent.extra \
    build@repo.data.kit.edu \
    gpg -K
```

This MUST output something like this:

```sh
/home/build/.gnupg/pubring.kbx
------------------------------
sec#  rsa3072 2018-11-15 [SC]
      ACDFB08FDC962044D87FF00B512839863D487A87
uid           [ unknown] Debian Packages (packages.data.kit.edu) <packages@lists.kit.edu>
```

## Copy packages and sign

Use the package-mover to (copy: `-c`, sign: `--sign`) it for you:

You should fist take a look (`-v` instead of `-c`)

```sh
ssh -R /run/user/1000/gnupg/S.gpg-agent:/run/user/1000/gnupg/S.gpg-agent.extra \
    build@repo.data.kit.edu \
    ~cicd/ci-voodoo/ci-tools/package-mover.sh -s preprod -t / -c -p <package name> --sign
```

## Sign repo

Or use this commandline to sign the prod repo:

```sh
ssh -R /run/user/1000/gnupg/S.gpg-agent:/run/user/1000/gnupg/S.gpg-agent.extra \
    build@repo.data.kit.edu \
    "~cicd/ci-voodoo/ci-tools/sign-all-repos.sh -t / -k prod"
```

This command can also sign any other repo, then don't specify the prod key
(i.e. omit `-k prod`)

