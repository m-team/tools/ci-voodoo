# CI VOODOO

[Repositories are documented here](repositories.md)

This contains all you need for generic ci stuff.

Just place these two lines into your gitlab.ci: This will run builds for
all supported distributions

```yaml
include:
  - 'https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo/raw/master/ci-include/generic-ci.yml'
  - 'https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo/raw/master/ci-include/pipeline-jobs.yml'
```


# When will which job run:

is defined in `ci-include/distro-build-conditions.yml`

Note: Merges are pushes, too.

# Repository

All build results will be available in the staging repository
[https://repo.data.kit.edu/staging](https://repo.data.kit.edu/staging)

You'll need to rebuild the repository index using the
`update-debian-repo.sh` tool from
[cowbuilder-scripts](https://github.com/marcvs/cowbuilder-scripts)


# github status sync

To use it include the following in the .gitlab-ci.yml (after it is merged):

```yaml
include:
  - 'https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo/-/raw/master/ci-include/github-status-sync.yml'

variables:
  UPSTREAM_PROJECT: ORG/REPO
```

where ORG/REPO is replaced with the name of the organization/user and repository on github.

You also need to add an github personal access token with the repo:status scope (read+write) to Codebase. This is done as a env var (Settings->CI/CD->Variables). The name MUST be GH_STATUS_TOKEN.

If you have multiple repos within the same github organization that has a corresponding subgroup on codebase, you might want to create an access token for this organization that gives access to all or multiple repos at the same time and then can set this access token as a variable in the subgroup - this way you don't have to set it in the individual repos.


# DOs and DONTs 

## DO

- Define a `script` section with all you need to do for building. Defaults are
    - `deb`-based distros:
    ```
          make debsource
          dpkg-buildpackage -uc -us
    ```
    - `rpm`-based distros:
    ```
        make  rpmsource # if that target exists
        make rpms
    ```

- If you create a `before_script` to prepare your build
    - you MAY therein set the prerel version, if you're on prerel
    - you SHOULD call the default-before-script by adding this as the last
        line in your own `before_script`:
    ```yaml
      - !reference [.default-before-script]
    ```
- If you define your own `after_script`, plase make sure to put your
    results into the right folder (consult `generic-ci.yml`, 
     or place files info `results/${DISTRO}/${RELEASE}`

## DONT

- Add more than one `.on-*` extensions -- unless you love unpredictability
- Define (i.e. overwrite) the `after_script` (unless
    you've checked what the original does in `generic-ci.yml`.
- Anything that exposes secrets in the web frontend is evil. A simple
    `env` could expose private keys from environment variables!



# Examples

See the [ci-include/pipeline-jobs.yml](ci-include/pipeline-jobs.yml) file for examples.

# Repositories

We'll now have **four** repositories:

## devel
Policy:
- Any branch (except "prerel" and "master" (or "main")):
- Any commit
- Subset of distros
- Overwrite: true
- Signature: automatic
- Integration-test installs from artifact
- Depenencies are resolved from **prerel**
- If successful, package is stored in **devel**

## **prerel**
This repo should be used for providing access to early versions to development partners

Policy:
- Branch: "prerel"
- All supported distros
- Overwrite: false
- Call ".gitlab-ci-scripts//set-prerel-version" if it exists
- Signature: automatic
- Integration-test installs from artifact (as above)
- Depenencies are resolved from **prerel** (as above)
- If successful, package is stored in **prerel**


## preprod
Policy:
- Branch: "master" or tagged version (nyi)
- All supported distros
- Overwrite: false
- Only successfully integration tested packages
- Dynamic versioning: not supported
- Signature: automatic
- Integration-test installs from artifact (as above)
- Depenencies are resolved from **prerel** (as above) => **prod** (if discussion converged)
- If successful, package is stored in **preprod** (dont-overwrite policy)
- Packages should me **moved** from here to prod

## **prod**
Policy: same as `preprod`, but:
- Only manually copied packages from preprod
- Signature: manual

PS: This behaviour is confiured in https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo/-/blob/master/ci-include/global-defaults.yml and should be overridable in a project's .gitlab-ci.yml file


# Configuration options

## Variables

There's a bunch of variables that will be used to influence the whole
build process:

- `DOCKER_IMAGE_NAMESPACE`: 'marcvs/build'
- `DOCKER_IMAGE_NAME`: 'oidc-agent'
- `GIT_STRATEGY`: clone
- `GIT_DEPTH`: 0
- `CLEAN_FILES`: ".gitlab-ci.yml .gitlab-ci-scripts rpm"
- `SPEC_FILE`: "packaging/rpm/mustach.spec"
- `PREREL_BRANCH_NAME`: 'scc/1.2.5'
- `MTEAM_CI_EXTRA_REPOSITORY_STRING`: 'deb https://repo.data.kit.edu/devel/${DISTRO}/${RELEASE} ./'
- `MTEAM_CI_EXTRA_REPOSITORY_KEY_URL`: 'https://repo.data.kit.edu/devel/automatic-repo-data-kit-edu-key.gpg'
- `MTEAM_CI_EXTRA_REPOSITORY_URL_YUM`: 'https://repo.data.kit.edu/devel/data-kit-edu-${DISTRO}${RELEASE}.repo'
- `MTEAM_CI_EXTRA_REPOSITORY_URL_ZYPPER`: 'https://repo.data.kit.edu/devel/data-kit-edu-${DISTRO}${RELEASE}.repo'
- `MTEAM_CI_ADDITIONAL_PACKAGES_APT`: 'valgrind mustache-spec pkgconf libjson-c-dev libcjson-dev libjansson-dev'
- `MTEAM_CI_ADDITIONAL_PACKAGES_YUM`: 'valgrind json-c-devel jansson-devel'
- `MTEAM_CI_ADDITIONAL_PACKAGES_ZYPPER`: 'valgrind '

## Scripts

There's also several scripts, that will be called, if they're present in
you repositories `.gitlab-ci-scripts` folder.
Note that the `ci-voodoo/ci-tools` contains defaults for these scripts. If
you place them into your project's `.gitlab-ci-scripts` folder, those will
not be called!

- `add-extra-repository.sh`
- `set-prerelease-version.sh`

# Triggering pipelines

See <https://codebase.helmholtz.cloud/m-team/archive/ci-testing/parent>
for an example.

Triggering pipelines (in the community edition (CE)) is available via:
```yaml
include:
  - 'https://codebase.helmholtz.cloud/m-team/tools/ci-voodoo/raw/master/ci-include/parent-child.yml'
```
This include is already there, if you use `.../ci-include/generic-ci.yml`.

You need a `TRIGGER_TOKEN`: for the triggered (child) project. 
1. Create it: in the child project under "settings -> ci/cd -> Pipeline trigger tokens"
    - Name it like the parent project (e.g. `oidc-ssh_motley-cue_trigger`)
2. In the parent project unddr "settings -> ci/cd -> Variables" store
   it as "TRIGGER_TOKEN_<TRIGGERED_PROJECT_NAME>" (eg. `trigger_token_BUILD_ALL_OIDC_AGENT`)
   - You **MUST** MASK the token!

You also need an `API_TOKEN`: for the child project, so the parent can
query its status.
1. Create it: in the child project under "settings -> Access Tokens"
    - Name it like the parent project (e.g. `oidc-ssh_motley-cue_api`)
    - No expiry (unless you love unexpected failures)
    - Role: "developer"
    - Scopes: "read_api"
2. In the parent project unddr "settings -> ci/cd -> Variables" store
   it as "API_TOKEN_<TRIGGERED_PROJECT_NAME>" (e.g. `API_TOKEN_oidc_agent`)
   - You **MUST** MASK the token!


## Usage
```yaml
  script:
    - !reference [.def-trigger-pipeline]
    - trigger_pipeline --project-name ${DWNSTRM_PROJECT_NAME} \
                       --branch ${TRIGGER_BRANCH} \
                       --trigger-token ${TRIGGER_TOKEN_<PROJECT_NAME>} \
                       --api-token ${API_TOKEN_<PROJECT_NAME>} \
                       --<ANY_OTHER_VARIABLE_NAME> "That you need in the triggered pipeline"
     # Returns: TRIGGERED_PIPELINE_ID
```

# Triggered pipelines

See <https://codebase.helmholtz.cloud/m-team/archive/ci-testing/child> for
an example.

Triggered pipelines will need artifacts of **jobs** in the upstream
**pipeline**. Each job creates its own pipelines.

**NOTE**: the current mechanism can only access parent pipeline artifacts
from **public** projects.

```yaml
  script:
    - !reference [.def-get-artifacts]
    - |
      # get_artifacts "testjob" output
      get_artifacts --upstream-job-name ${CI_JOB_NAME} \ # required, use $CI_JOB_NAME if same name as current job
                    --upstream-project-name "A name" \ # optional
                    --upstream-project-id "project ID" \ # optional
                    --upstream-pipeline-id "pipeline ID" \ # optional
                    --output <FOLDER> \ # optional
```
The first parameter is the name of the upstream job.
The second parameter is the name of the folder where the artifacts are being put.
